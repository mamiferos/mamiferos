package br.ufrj.macae.constante;

public class DarwinCoreTerms {
	
	//OCCURRENCE	
	public static final String OCCURRENCE = NameSpace.URI_DWC +  "Occurrence";
	public static final String CATALOG_NUMBER = NameSpace.URI_DWC + "catalogNumber";
	public static final String RECORD_NUMBER = NameSpace.URI_DWC + "recordNumber";
	public static final String OCCURRENCE_REMARKS = NameSpace.URI_DWC + "occurrenceRemarks";
	public static final String SEX = NameSpace.URI_DWC +  "sex";
	public static final String PREPARATIONS = NameSpace.URI_DWC + "preparations";
	public static final String DISPOSITION = NameSpace.URI_DWC + "disposition";
	public static final String RECORDED_BY = NameSpace.URI_DWC + "recordedBy";	
	 
	//Termos de relação
	public static final String AT_EVENT = NameSpace.URI_DSW+ "atEvent";
	public static final String EVENT_OF = NameSpace.URI_DSW+ "EventOf";
	public static final String LOCATE_AT = NameSpace.URI_DSW+ "locateAt";
	

	//Event
	public static final String EVENT = NameSpace.URI_DWC + "Event";
	public static final String SAMPLING_PROTOCOL = NameSpace.URI_DWC + "samplingProtocol";
	public static final String EVENT_REMARKS = NameSpace.URI_DWC + "eventRemarks";
	public static final String FIELD_NUMBER = NameSpace.URI_DWC + "fieldNumber";
	public static final String EVENT_DATE = NameSpace.URI_DWC + "eventDate";
	public static final String FIELD_NOTES = NameSpace.URI_DWC + "fieldNotes";
	
	//Location
	public static final String LOCATION = NameSpace.URI_DWC + "Location";
	public static final String STATE_PROVIDENCE = NameSpace.URI_DWC + "stateProvidence";
	public static final String STATE = NameSpace.URI_DWC + "state";
	public static final String MUNICIPALITY = NameSpace.URI_DWC + "municipality";
	public static final String COUNTY = NameSpace.URI_DWC + "county";
	public static final String LOCATION_REMARKS = NameSpace.URI_DWC + "locationRemarks";
	public static final String LOCALITY = NameSpace.URI_DWC + "locality";
	public static final String DECIMAL_LONGITUDE = NameSpace.URI_DWC + "decimalLongitude";
	public static final String DECIMAL_LATITUDE = NameSpace.URI_DWC + "decimalLatitude";
	public static final String IN_DESCRIBED_PLACE = NameSpace.URI_DWC_IRI + "inDescribedPlace";
	
	
	//MeasurementOrFact
	public static final String MEASUREMENT_OR_FACT = NameSpace.URI_DWC + "MeasurementOrFact";
	public static final String MEASUREMENT_TYPE = NameSpace.URI_DWC + "measurementType";	
	public static final String MEASUREMENT_VALUE = NameSpace.URI_DWC + "measurementValue";
	public static final String MEASUREMENT_UNIT = NameSpace.URI_DWC + "measurementUnit";
	public static final String MEASUREMENT_REMARKS = NameSpace.URI_DWC + "measurementRemarks";
	 
	
	//Taxon
	public static final String TO_TAXON = NameSpace.URI_DWC_IRI + "toTaxon";
	
	//Identification
	public static final String IDENTIFICATION = NameSpace.URI_DWC + "Identification";
	public static final String SCIENTIFIC_NAME = NameSpace.URI_DWC + "scientificName";
	public static final String ORDER = NameSpace.URI_DWC + "order";
	public static final String FAMILY = NameSpace.URI_DWC + "family";
	
	
}
