package br.ufrj.macae.constante;

public class NameSpace {
	
	public static final String URI_BASE = "http://mamiferos.macae.ufrj.br/";
	public static final String URI_DWC = "http://rs.tdwg.org/dwc/terms/";
	public static final String URI_DWC_IRI = "http://rs.tdwg.org/dwc/iri/";  
    public static final String URI_DCTERMS = "http://purl.org/dc/terms/";
    public static final String URI_DSW = "http://purl.org/dsw/";  
	
}
