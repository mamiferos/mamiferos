/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufrj.macae.controller;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.RowEditEvent;

import br.ufrj.macae.entity.MeasurementOrFact;
import br.ufrj.macae.entity.Occurrence;
import br.ufrj.macae.tic.util.Mensagem;


@Named
@ViewScoped
public class MeasurementMB implements Serializable {		
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8930371778192242404L;

	@Inject
	private MeasurementOrFact measurement;
	
	@Inject
	private MeasurementOrFact measurementSave;
	
	@Inject
	private Occurrence occurrence;
	
	private List<MeasurementOrFact> measurementList;
	
	

	

public MeasurementOrFact getMeasurement() {
		return measurement;
	}

	public void setMeasurement(MeasurementOrFact measurement) {
		this.measurement = measurement;
	}

	public MeasurementOrFact getMeasurementSave() {
		return measurementSave;
	}

	public void setMeasurementSave(MeasurementOrFact measurementSave) {
		this.measurementSave = measurementSave;
	}

	public List<MeasurementOrFact> getMeasurementList() {
		return measurementList;
	}

	public void setMeasurementList(List<MeasurementOrFact> measurementList) {
		this.measurementList = measurementList;
	}
	

	public Occurrence getOccurrence() {
		return occurrence;
	}

	public void setOccurrence(Occurrence occurrence) {
		this.occurrence = occurrence;
	}

@PostConstruct	
   public void init() {
	
	  	 
   } 

public void save() {
	   
	   try {
		   		   
		 measurementList.add(measurementSave); 
		Mensagem.adicionarMensagemSucesso("Medida cadastrada com sucesso!");
		
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		//Mensagem.adicionarMensagemErro("Ocorreu um erro inesperado, por favor, contacte nosso suporte.");
	}
	   
}
  
   
   public void edit(RowEditEvent event) {
	   
		 try {			 
			 
		     
			 FacesMessage msg = new FacesMessage("Medida atualizada com sucesso!");
		     FacesContext.getCurrentInstance().addMessage(null, msg);
			//Mensagem.adicionarMensagemSucesso("Medida atualizada com sucesso!");
			
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			//Mensagem.adicionarMensagemErro("Ocorreu um erro inesperado, por favor, contacte nosso suporte.");
			 
		}
}
 
 public void delete() {
	   
		 try {			 
			 
			 
			Mensagem.adicionarMensagemSucesso("Medida removida com sucesso!");
			measurementList.remove(measurement);
			
			/*
			for(MeasurementOrFact m : measurementList) {
				//if(m.getMeasurementValue()
			}*/
			
			
		} catch (Exception e) {
			e.printStackTrace();
			
			//Mensagem.adicionarMensagemErro("Ocorreu um erro inesperado, por favor, contacte nosso suporte.");
			 
		}
}

 
 public String back() {
	  
	   
	   //organismList = organism.getOccurrence().getOrganismList();
		 
	return "listOrganism";
 }	
 
 public String openDel() {
	 
		//organism  = organismDAO.recupera(organism.getId());
		//measurementList = organism.getMeasurement();
		 
		return "listMeasurement";
}
 
 public void onRowEdit(RowEditEvent event) {
     FacesMessage msg = new FacesMessage("Editado com sucesso.");
     FacesContext.getCurrentInstance().addMessage(null, msg);
 }
  
 public void onRowCancel(RowEditEvent event) {
     FacesMessage msg = new FacesMessage("Atualização cancelada.");
     FacesContext.getCurrentInstance().addMessage(null, msg);
 }
 
 
}
