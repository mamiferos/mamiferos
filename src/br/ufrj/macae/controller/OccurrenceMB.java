/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufrj.macae.controller;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.jena.rdf.model.Resource;
import org.primefaces.event.RowEditEvent;

import br.ufrj.macae.dao.MeasurementDAO;
import br.ufrj.macae.dao.OccurrenceDAO;
import br.ufrj.macae.dao.SampleDAO;
import br.ufrj.macae.entity.Event;
import br.ufrj.macae.entity.MeasurementOrFact;
import br.ufrj.macae.entity.Occurrence;
import br.ufrj.macae.tic.util.Mensagem;
import br.ufrj.macae.tic.util.Util;


@Named
@ViewScoped
public class OccurrenceMB implements Serializable {	



	/**
	 * 
	 */
	private static final long serialVersionUID = -3223116181925696039L;

	private Occurrence occurrence;

	@Inject
	private Occurrence occurrenceSave;
		
	
	private List<Occurrence> occurrenceList; 
	
	private List<Occurrence> occurrenceListPesquisa; 
	
	private static OccurrenceDAO OCCURRENCE_DAO = new OccurrenceDAO();
	
	private static MeasurementDAO MEASUREMENT_DAO = new MeasurementDAO(); 
	
	private static SampleDAO SAMPLE_DAO = new SampleDAO();
	
	private List<Event> eventList;
	
	@Inject
	private MeasurementOrFact measurement;
	
	private List<MeasurementOrFact> measurementList; 

	private int pagina;
	
	private boolean last;
	private boolean first;
	
	public List<Event> getEventList() {
		return eventList;
	}

	public void setEventList(List<Event> eventList) {
		this.eventList = eventList;
	}

	public boolean isLast() {
		return last;
	}

	public void setLast(boolean last) {
		this.last = last;
	}

	public boolean isFirst() {
		return first;
	}

	public void setFirst(boolean first) {
		this.first = first;
	}

	public List<MeasurementOrFact> getMeasurementList() {
		return measurementList;
	}

	public void setMeasurementList(List<MeasurementOrFact> measurementList) {
		this.measurementList = measurementList;
	}

	public int getPagina() {
		return pagina;
	}

	public void setPagina(int pagina) {
		this.pagina = pagina;
	}

	public Occurrence getOccurrence() {
		return occurrence;
	}

	public void setOccurrence(Occurrence occurrence) {
		this.occurrence = occurrence;
	}

	public List<Occurrence> getOccurrenceList() {
		return occurrenceList;
	}

	public void setOccurrenceList(List<Occurrence> occurrenceList) {
		this.occurrenceList = occurrenceList;
	}

	public Occurrence getOccurrenceSave() {
		return occurrenceSave;
	}

	public void setOccurrenceSave(Occurrence occurrenceSave) {
		this.occurrenceSave = occurrenceSave;
	}
	

	public List<Occurrence> getOccurrenceListPesquisa() {
		return occurrenceListPesquisa;
	}

	public void setOccurrenceListPesquisa(List<Occurrence> occurrenceListPesquisa) {
		this.occurrenceListPesquisa = occurrenceListPesquisa;
	}
	
	public MeasurementOrFact getMeasurement() {
		return measurement;
	}

	public void setMeasurement(MeasurementOrFact measurement) {
		this.measurement = measurement;
	}

	@PostConstruct	
	public void init() {
		

		String uri = FacesContext.getCurrentInstance().getExternalContext()
				.getRequestParameterMap().get("uri");
		
		occurrenceSave.getEvent().setUri(uri);
				
	}
		
	
  public void save() {
		   
		try {
			
			if(OCCURRENCE_DAO.searchTombo(occurrenceSave.getCatalogNumber())) {
				Mensagem.adicionarMensagemErro("O número de tombo já se encontra cadastrado.");
			} else {
				
				OCCURRENCE_DAO.getIdentification(occurrenceSave);
				
				if( occurrenceSave.getOccurrence() != null && occurrenceSave.getOccurrence().isAnon() ) {
					OCCURRENCE_DAO.remove(occurrenceSave.getOccurrence());
				}
				
				Resource occurrenceRS = OCCURRENCE_DAO.insert(occurrenceSave);
				
				for( MeasurementOrFact m : occurrenceSave.getMeasurements() ) {
					
					m.setOccurrence(occurrenceRS);
					MEASUREMENT_DAO.save(m);
				}
				
				//eventList = SAMPLE_DAO.list();
				 
				Mensagem.adicionarMensagemSucesso("Espécime cadastrada com sucesso!");
				
				clear();
			}
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Mensagem.adicionarMensagemErro(Util.ERRO_INSERIR);
		}
  }
  
  public void addMeasurement() {	  
	  
	  
	  try {
		occurrenceSave.getMeasurements().add(measurement.clone());
	} catch (CloneNotSupportedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	  
  }
  
	public void pesquisa() {
		occurrenceListPesquisa.addAll(occurrenceList);
	}
	
	 public void onRowEdit(RowEditEvent event) {
		 
		// occurrenceSave.getMeasurements().add(measurement);
		 
	     FacesMessage msg = new FacesMessage("Editado com sucesso.");
	     FacesContext.getCurrentInstance().addMessage(null, msg);
	 }
	  
	 public void onRowCancel(RowEditEvent event) {
	     FacesMessage msg = new FacesMessage("Atualização cancelada.");
	     FacesContext.getCurrentInstance().addMessage(null, msg);
	 }
	 
	 public void delete() {
		   
		 try {			 
			 
			 
			Mensagem.adicionarMensagemSucesso("Medida removida com sucesso!");
			occurrenceSave.getMeasurements().remove(measurement);
			
			/*
			for(MeasurementOrFact m : measurementList) {
				//if(m.getMeasurementValue()
			}*/
			
			
		} catch (Exception e) {
			e.printStackTrace();
			
			//Mensagem.adicionarMensagemErro("Ocorreu um erro inesperado, por favor, contacte nosso suporte.");
			 
		}
}
 
	
 public void openOccurrence(Event event) {
	 
	 try {
		 
		 occurrenceSave = new Occurrence();
		 occurrenceSave.setEvent(event);		 
		 FacesContext.getCurrentInstance().getExternalContext().redirect("insertSpecimen.jsf?uri=" + event.getUri() );
		 
	 } catch (Exception e) {
		e.printStackTrace();
	}
	 
 }
 
 public String clear() {
	 
	 try {
		occurrence = occurrenceSave.clone();
		
		 occurrenceSave = new Occurrence();
		 occurrenceSave.setEvent(occurrence.getEvent());
		 measurementList = new ArrayList<MeasurementOrFact>();
		 occurrenceSave.setMeasurements(measurementList);
		 
	} catch (CloneNotSupportedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	 
	 return "insertSpecimen";
 }
 	 
 public void view(Event ev) {
	 
	 try {
		 
		 occurrenceSave.setEvent(ev);
		 
		 occurrenceList  = OCCURRENCE_DAO.view(occurrenceSave);
		 
		 for (Occurrence occurrence : occurrenceList) {
			 measurementList = MEASUREMENT_DAO.list(occurrence);
			 occurrence.setMeasurements(measurementList);
		 }
		 
		 if(!occurrenceList.isEmpty()) {
			 occurrence = occurrenceList.get(0);
		 }
		 
		 
		 if(occurrenceList.size() <= 1) {
			 first = false;
			 last = false;
		 } else {
			 first = true;
			 last = false;
		 }
		 
	 } catch (Exception e) {
		// TODO: handle exception
		 e.printStackTrace();
	}
 }
 
 public void next() {
	 
	 int i = occurrenceList.indexOf(occurrence);
	 if(i+1  < occurrenceList.size() - 1) {
		 pagina = i + 1;
		 
		 first = true;
		 
		 if(occurrenceList.size() == 2) {
			 last = false;
		 } else {
			 last = true;
		 }
		 
		 
		 occurrence = occurrenceList.get(i+1);
	 } else {
		 last = true;
		 first = false;
		 occurrence = occurrenceList.get(i+1);
	 }
	 
 }

public void back() {
	 
	 int i = occurrenceList.indexOf(occurrence);
	 if(i-1 > 0) {
		 pagina = i - 1;
		 
		 if(occurrenceList.size() == 2) {
			 last = false;
		 } else {
			 last = true;
		 }
		 
		 first = true;
		 
		 occurrence = occurrenceList.get(i-1);
	 } else {
		 first = true;
		 last = false;
		 occurrence = occurrenceList.get(i-1);
	 }
	 
 }


}
