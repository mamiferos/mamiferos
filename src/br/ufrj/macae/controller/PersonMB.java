/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufrj.macae.controller;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import br.ufrj.macae.entity.Researcher;
import br.ufrj.macae.tic.util.Mensagem;
import br.ufrj.macae.tic.util.Util;


@Named
@ViewScoped
public class PersonMB implements Serializable {			
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 5168758497714099707L;

	@Inject
	private Researcher person;
	
	@Inject
	private Researcher personSave;
			
	private List<Researcher> personList;
		

	public Researcher getPerson() {
		return person;
	}

	public void setPerson(Researcher person) {
		this.person = person;
	}

	public Researcher getPersonSave() {
		return personSave;
	}

	public void setPersonSave(Researcher personSave) {
		this.personSave = personSave;
	}

	public List<Researcher> getPersonList() {
		return personList;
	}

	public void setPersonList(List<Researcher> personList) {
		this.personList = personList;
	}
		
	

	public boolean isLogado() {
		FacesContext fc = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) fc.getExternalContext().getSession(true);
		personSave = (Researcher) session.getAttribute("person");
		
		if(personSave == null) {
			return false;
		}
		
		return true;
	}


	@PostConstruct	
   public void init() {
	 
   } 

public void save() {
	   
	   try {
		   		   ;
				
		//Mensagem.adicionarMensagemSucesso("Medida cadastrada com sucesso!");
		
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		//Mensagem.adicionarMensagemErro("Ocorreu um erro inesperado, por favor, contacte nosso suporte.");
	}
	   
}
  
   
   public void edit() {
	   
		 try {			 
			 
		     
			//Mensagem.adicionarMensagemSucesso("Medida atualizada com sucesso!");
			
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			//Mensagem.adicionarMensagemErro("Ocorreu um erro inesperado, por favor, contacte nosso suporte.");
			 
		}
}
    
 
 public void delete() {
	   
		 try {			 
			 
			 
			//Mensagem.adicionarMensagemSucesso("Medida removida com sucesso!");
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
			
			//Mensagem.adicionarMensagemErro("Ocorreu um erro inesperado, por favor, contacte nosso suporte.");
			 
		}
}

 
 public String back() {
	  
	   
	   //organismList = organism.getOccurrence().getOrganismList();
		 
	return "listOrganism";
 }	
 
 public String openDel() {
	 
		//organism  = organismDAO.recupera(organism.getId());
		//measurementList = organism.getMeasurement();
		 
		return "listMeasurement";
}
 
	public void logar() {

		try {

			//person.setPassword(Util.criptografaSenha(person.getKey()));
			//person = personDAO.autenticarUsuario(person);

			// guarde o usuário na sessão
			FacesContext fc = FacesContext.getCurrentInstance();
			HttpSession session = (HttpSession) fc.getExternalContext().getSession(true);
			session.setAttribute("person", person);
						
			FacesContext.getCurrentInstance().getExternalContext().redirect("../public/insertSample.jsf");
			

		} 
		//catch (LoginException e) {

			//FacesContext.getCurrentInstance().addMessage(null,
			//		new FacesMessage(FacesMessage.SEVERITY_INFO, e.getMessage(), null));
		//}
		catch (Exception e) {
			e.printStackTrace();

			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "", null));
		}

	}

	public void logout() {

		try {

			// guarde o usuaário na sessão
			FacesContext fc = FacesContext.getCurrentInstance();
			HttpSession session = (HttpSession) fc.getExternalContext().getSession(false);
			session.invalidate();
						
			FacesContext.getCurrentInstance().getExternalContext().redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/index.xhtml");

		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "", null));
		}

	}

	 
 
}
