/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufrj.macae.controller;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import br.ufrj.macae.dao.SampleDAO;
import br.ufrj.macae.entity.Event;
import br.ufrj.macae.entity.Identification;
import br.ufrj.macae.entity.Location;
import br.ufrj.macae.entity.Recorder;
import br.ufrj.macae.entity.SamplingProtocol;
import br.ufrj.macae.tic.util.Mensagem;
import br.ufrj.macae.tic.util.SamplingProtocolEnum;
import br.ufrj.macae.tic.util.Util;


@Named
@SessionScoped
@ManagedBean
public class SampleMB implements Serializable {	
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -6220015701994728351L;

	private Event event;
		
	@Inject
	private Event eventSave;
	
	private List<Event> eventList;
	
	private List<Event> eventListPesquisa;
	
	private List<SamplingProtocolEnum> samplingProtocolList;
	
	private List<SamplingProtocol> samplingProtocolSelect = new ArrayList<SamplingProtocol>();	
	
	private List<Recorder> recorderSelect = new ArrayList<Recorder>();
	
	private static SampleDAO SAMPLE_DAO = new SampleDAO();
	
	private Recorder recordedBy = new Recorder();	
	
	@Inject
	private SamplingProtocol samplingProtocol;	
	
	private List<String> species;
	
	private List<Identification> identificationList;
	private List<Identification> orderList;
	private List<Identification> familyList;
	
	private List<Location> stateList;
	private List<Location> countyList;
	
	private Date initialDate;
	private Date finalDate;
	
	private String protocol;
	
	private int filtro;
	
	private int page = 0;


	public int getPage() {
		return page;
	}


	public void setPage(int page) {
		this.page = page;
	}
	
	
	public int getFiltro() {
		return filtro;
	}


	public void setFiltro(int filtro) {
		this.filtro = filtro;
	}


	public List<Location> getStateList() {
		return stateList;
	}


	public void setStateList(List<Location> stateList) {
		this.stateList = stateList;
	}


	public List<Location> getCountyList() {
		return countyList;
	}


	public void setCountyList(List<Location> countyList) {
		this.countyList = countyList;
	}


	public List<Identification> getOrderList() {
		return orderList;
	}


	public void setOrderList(List<Identification> orderList) {
		this.orderList = orderList;
	}


	public List<Identification> getFamilyList() {
		return familyList;
	}


	public void setFamilyList(List<Identification> familyList) {
		this.familyList = familyList;
	}


	public Date getInitialDate() {
		return initialDate;
	}


	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}


	public Date getFinalDate() {
		return finalDate;
	}


	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}


	public Recorder getRecordedBy() {
		return recordedBy;
	}


	public void setRecordedBy(Recorder recordedBy) {
		this.recordedBy = recordedBy;
	}

	public List<String> getSpecies() {
		return species;
	}

	public void setSpecies(List<String> species) {
		this.species = species;
	}


	public List<Recorder> getRecorderSelect() {
		return recorderSelect;
	}



	public void setRecorderSelect(List<Recorder> recorderSelect) {
		this.recorderSelect = recorderSelect;
	}



	public List<SamplingProtocol> getSamplingProtocolSelect() {
		return samplingProtocolSelect;
	}



	public void setSamplingProtocolSelect(List<SamplingProtocol> samplingProtocolSelect) {
		this.samplingProtocolSelect = samplingProtocolSelect;
	}



	public SamplingProtocol getSamplingProtocol() {
		return samplingProtocol;
	}



	public void setSamplingProtocol(SamplingProtocol samplingProtocol) {
		this.samplingProtocol = samplingProtocol;
	}



	public Event getEvent() {
		return event;
	}



	public void setEvent(Event event) {
		this.event = event;
	}



	public Event getEventSave() {
		return eventSave;
	}



	public void setEventSave(Event eventSave) {
		this.eventSave = eventSave;
	}



	public List<Event> getEventList() {
		return eventList;
	}



	public void setEventList(List<Event> eventList) {
		this.eventList = eventList;
	}



	public List<SamplingProtocolEnum> getSamplingProtocolList() {
		return samplingProtocolList;
	}



	public void setSamplingProtocolList(List<SamplingProtocolEnum> samplingProtocolList) {
		this.samplingProtocolList = samplingProtocolList;
	}


	public List<Event> getEventListPesquisa() {
		return eventListPesquisa;
	}


	public void setEventListPesquisa(List<Event> eventListPesquisa) {
		this.eventListPesquisa = eventListPesquisa;
	}
	

	public List<Identification> getIdentificationList() {
		return identificationList;
	}


	public void setIdentificationList(List<Identification> identificationList) {
		this.identificationList = identificationList;
	}
	
	


	public String getProtocol() {
		return protocol;
	}


	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}


	@PostConstruct
	public void init() {
		
		try {
			
		
		eventList = SAMPLE_DAO.list();	
		
		searchOrder();
		searchFamily();
		searchState();
		
		samplingProtocolList = Arrays.asList(SamplingProtocolEnum.values());
		
		// se o tamanho da lista for <= 1 não irá aparecer a navegação
		/*
		if(eventList.size() <= 1) {		
			first = 1;
			last = 1;
		}*/
		} catch(Exception e) {
			
			e.printStackTrace();
		}
	}
	
	
	public List<String> searchSpecies(String query) {		
		return SAMPLE_DAO.searchSpecies(query);
	}
	
	public void onSpecieSelect() {
		
		//SAMPLE_DAO.searchSpecie(eventSave.getIdentification());		
	}
	
	public void onLocaleSelect() {
		
		//SAMPLE_DAO.searchSpecie(eventSave.getIdentification());
		
	}
	
	
	public void insertProtocol() {
		
		SamplingProtocol sp = new SamplingProtocol();
		sp.setSamplingProtocol(samplingProtocol.getSamplingProtocol());
		
		samplingProtocolSelect.add(sp);
		eventSave.setSamplingProtocol(samplingProtocolSelect);
	}

	public void removeProtocol(SamplingProtocol sp) {
		samplingProtocolSelect.remove(sp);
		eventSave.setSamplingProtocol(samplingProtocolSelect);
	}

	public void insertRecorder() {
		
		Recorder r = new Recorder();
		r.setName(recordedBy.getName());
		
		recorderSelect.add(r);
		eventSave.setRecordedByList(recorderSelect);
	}

	public void save() {
	
		try {
			
			//SAMPLE_DAO.searchLocalGeonames(eventSave.getLocation());
			//SAMPLE_DAO.searchUriDBPedia(eventSave.getIdentification());
			
			
			String taxon[] = eventSave.getIdentification().getToTaxon().split(";");
			eventSave.getIdentification().setToTaxon(taxon[0]); 
			eventSave.getIdentification().setScientificName(taxon[1]);
			
			getSpecieGBif(eventSave.getIdentification());
			
			eventSave.setSamplingProtocol(samplingProtocolSelect);
			
			//String contexto = FacesContext.getCurrentInstance().getExternalContext().getApplicationContextPath();
			//FacesContext.getCurrentInstance().getExternalContext().redirect(contexto + "/public/insertSpecimen.jsf?uri=" + eventSave.getUri() );
						
			SAMPLE_DAO.save(eventSave);
			
			
			//clear();			
			//FacesContext.getCurrentInstance().getExternalContext().redirect(contexto + "/public/insertSpecimen.jsf?uri=" + '1' );
			
			//Mensagem.adicionarMensagemSucesso();
			
			
			
			
		} catch (Exception e) {					
			Mensagem.adicionarMensagemErro();
			e.printStackTrace();			
		}
		
	}
	
	public void clear() {
		 
		 try {
			 
			 event = eventSave.clone();		
			 eventSave = new Event();
			 eventSave.setIdentification(new Identification());
			 eventSave.setLocation(new Location());
			 event.getSamplingProtocol().clear();
			 			 
			 eventSave.setSamplingProtocol(event.getSamplingProtocol());		 
			 
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		 
	}

	
public String viewSample(Event ev) {
	
try {
		
		SAMPLE_DAO.view(ev);
		
		/*
		int cont = 0;
		for (SamplingProtocol s : ev.getSamplingProtocol()) {
			
			if(cont == 0) {
				protocol = s.getSamplingProtocol();
			} else {
				protocol = protocol + " - " + s.getSamplingProtocol();
			}
			
			cont++;
		} 
		*/
		
		
		posElement = eventList.indexOf(ev);
		
		event = ev;
		
	} catch (Exception e) {
		e.printStackTrace();
		
	}
	
	return "viewDetails"; 
}

private void getSpecieGBif(Identification specie) throws Exception {
	

	try {
		
	
	String uri = null;
	String urlApi = "http://api.gbif.org/v1/species?name=" + URLEncoder.encode(specie.getScientificName(), "UTF-8");
	
	HttpURLConnection con = null;
	URL url = new URL(urlApi);
	con = (HttpURLConnection) url.openConnection();
	
//	InputStream content = con.getInputStream();
	
	
	con.setRequestMethod("GET");
	con.connect();
	
	if( 200 == con.getResponseCode()) {
		
		String json = getJson(url);

		JsonParser parser = new JsonParser();
		JsonObject obj = (JsonObject) parser.parse(json);
		
		JsonArray key = obj.getAsJsonArray("results");
		
		JsonElement e = key.get(0);
		obj = e.getAsJsonObject();
		uri = obj.get("key").toString();
		
		uri = "https://www.gbif.org/species/" + uri;
		
		specie.setSeeAlso(uri);
		
	} 
	
	} catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
	}
	
}

public static String getJson(URL url) {
	if (url == null)
		throw new RuntimeException("URL é null");

	String html = null;
	StringBuilder sB = new StringBuilder();
	try (BufferedReader bR = new BufferedReader(new InputStreamReader(url.openStream()))) {
		while ((html = bR.readLine()) != null)
			sB.append(html);
	} catch (Exception e) {
		e.printStackTrace();
	}

	return sB.toString();
}

public void open() {
	
	try {
				
		filtro = 0;
		eventList = SAMPLE_DAO.list();
		page = 0;
		FacesContext.getCurrentInstance().getExternalContext().redirect("../public/home.jsf");
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}

public String searchForPeriod() {
	
	String inicio = Util.formatarDataIso8601(initialDate);
	String fim = Util.formatarDataIso8601(finalDate);
	
	eventList = SAMPLE_DAO.searchForPeriod(inicio, fim);
	
	filtro = 1;
	
	return "home";
	
}

public void searchOrder() {
	
	orderList = SAMPLE_DAO.searchOrderDBPedia();
		
}

public void searchFamily() {
	
	//familyList = SAMPLE_DAO.searchFamilyDBPedia(eventSave.getIdentification().getOrder());
	familyList = SAMPLE_DAO.searchFamilyDBPedia();
		
}

public void searchIdentification() {
	
	identificationList = SAMPLE_DAO.searchSpeciesDBPedia(eventSave.getIdentification().getFamily());
		
}

public String searchForIdentification() {
	
	eventList = SAMPLE_DAO.searchForIdentification(eventSave.getIdentification());
	
	filtro = 2;
	
	return "home";
	
}

public void searchState() {
	
	stateList = SAMPLE_DAO.getStates();
		
}

public void searchCounty() {
	
	countyList = SAMPLE_DAO.getCounty(eventSave.getLocation().getStateProvidence());
		
}

public String searchForLocation() {
		
	eventList = SAMPLE_DAO.searchForMunicipality(eventSave.getLocation());
	
	filtro = 3;
	
	return "home";
	
}

public String pagination (int page) {
	
	eventList = SAMPLE_DAO.list(page);
	
	this.page = page;
	
	return "home";
}

private int posElement = 1;
private int first = 0;
private int last = 0;

public int getElement() {
	return posElement;
}


public void setElement(int element) {
	this.posElement = element;
}


public int getFirst() {
	return first;
}


public void setFirst(int first) {
	this.first = first;
}


public int getLast() {
	return last;
}


public void setLast(int last) {
	this.last = last;
}

public void navDetails(int nav) {
	
	//1 = próximo
	if(nav == 1) {
		
		if( (eventList.size())  >  (++posElement) ) { 
			event = eventList.get(posElement);
			SAMPLE_DAO.view(event);
		} else {
			
			page++;
			eventList = SAMPLE_DAO.list(page);
			
			if(eventList == null || eventList.isEmpty()) {
				
				Mensagem.adicionarMensagemSucesso("Última espécie.");
				page--;
				
			} else {
				posElement = 0;
				event = eventList.get(posElement);
				SAMPLE_DAO.view(event);
			}			
		}
		
		if( (eventList.size())  <=  (posElement) ) {
			last = 1;
		} else {
			last = 0;
		}
		
		
		
	} else {
		
		if( (eventList.size())  >  (posElement - 1) && posElement > 0 ) { 
			event = eventList.get(--posElement);
			SAMPLE_DAO.view(event);
		} else {
			
			if(page >= 1) {
				page--;	
			} else {
				Mensagem.adicionarMensagemSucesso("Primeira espécie.");
				return;
			}			
			
			eventList = SAMPLE_DAO.list(page);			
			if(eventList == null || eventList.isEmpty()) {
				
				Mensagem.adicionarMensagemSucesso("Primeira espécie.");
				
			} else {
				posElement = 5;
				event = eventList.get(posElement);
				SAMPLE_DAO.view(event);
			}
				
		}
		
		if(posElement == 0) {
			first = 1;
		} 
		
		
	}
	
	//return "viewDetails"; 
	
}


}
	 
