package br.ufrj.macae.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Selector;
import org.apache.jena.rdf.model.SimpleSelector;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.vocabulary.RDF;

import com.franz.agraph.jena.AGGraph;
import com.franz.agraph.jena.AGGraphMaker;
import com.franz.agraph.jena.AGModel;
import com.franz.agraph.repository.AGCatalog;
import com.franz.agraph.repository.AGRepository;
import com.franz.agraph.repository.AGRepositoryConnection;
import com.franz.agraph.repository.AGServer;

public class GenericDAO {
	
	 	//private static final String HOST = getenv("AGRAPH_HOST", "localhost");
	    private static final String HOST = getenv("AGRAPH_HOST", "10.77.0.104");
	    private static final String PORT = getenv("AGRAPH_PORT", "10035");
	    
	    public static final String SERVER_URL = "http://" + HOST + ":" + PORT;
	    public static final String CATALOG_ID = "";
	    public static final String REPOSITORY_ID = "mamiferos";
	    public static final String USERNAME = getenv("AGRAPH_USER", "root");
	    public static final String PASSWORD = getenv("AGRAPH_PASS", "tic123");
	    
	    private static final AGRepositoryConnection conn = getConnection();
	    protected static final AGModel model = getModel();
	    
	    private Statement stmt; 
	    
	
	    /**
	     * Gets the value of an environment variable.
	     * @param name Name of the variable.
	     * @param defaultValue Value to be returned if the varaible is not defined.
	     * @return Value.
	     */
	    private static String getenv(final String name, final String defaultValue) {
	    	final String value = System.getenv(name);
	        return value != null ? value : defaultValue;
	    }
	    
	    
	    private static final AGRepositoryConnection getConnection() {
	    	
	    	AGServer server = new AGServer(SERVER_URL, USERNAME, PASSWORD);
	    		    	
	        AGCatalog catalog = server.getCatalog(CATALOG_ID);
	        AGRepository myRepository = catalog.openRepository(REPOSITORY_ID);
			myRepository.initialize();
						
			return myRepository.getConnection();
			
	    }
	    
	    private static final AGModel getModel() {
	    	
	    	AGGraphMaker maker = new AGGraphMaker(conn);
			AGGraph graph = maker.getGraph();
			return new AGModel(graph);
			
	    }
	    
	    protected void addUri( String subject, String property, String object )
		{
			
			
			try
			{
				
				stmt = createStatementUri(subject, property, object);
				
				model.add( stmt );
			} catch (Exception e) {
				e.printStackTrace();
			} 
			
		}
		
		protected void add( String subject, String property, String object )
		{
			
			
			try
			{	
				
				stmt = createStatement(subject, property, object);
				
				model.add( stmt );
			} catch (Exception e) {
				e.printStackTrace();
			} 
			
		}
		
		protected void add( Resource resource, Property property, String value )
		{	
			try
			{
				 	
				model.add(resource, property, value);
				
				 
			} catch (Exception e) {
				e.printStackTrace();
			} 
			
		}
		
		protected void add( Resource resource, String property, String value, XSDDatatype dataType )
		{	
			try
			{
				 	
				model.add(resource, 
						model.createProperty(property), 
						model.createTypedLiteral(value, dataType)
						);
				
				 
			} catch (Exception e) {
				e.printStackTrace();
			} 
			
		}
		
		protected void add( Resource resource, String property, String value)
		{	
			try
			{
				 	
				model.add(resource, 
						model.createProperty(property), 
						model.createLiteral(value)
						);
				
				 
			} catch (Exception e) {
				e.printStackTrace();
			} 
			
		}
		
		protected void add( String resource, String property, Resource value )
		{	
			try
			{
				 	
				model.add(model.createResource(resource), 
						model.createProperty(property), 
						value
						);
				
				 
			} catch (Exception e) {
				e.printStackTrace();
			} 
			
		}
		
		protected void add( Resource resource, String property, Resource value )
		{	
			try
			{
				 	
				model.add(resource, 
						model.createProperty(property), 
						value);
				
				 
			} catch (Exception e) {
				e.printStackTrace();
			} 
			
		}
		
		protected void add( Resource resource, Property property, Resource value )
		{	
			try
			{
				 	
				model.add(resource, property, value);
				
				 
			} catch (Exception e) {
				e.printStackTrace();
			} 
			
		}
		
				
		private Statement createStatementUri(String subject, String property, String object) {
						
			stmt = model.createStatement
							 ( 	
								model.createResource( subject ), 
								model.createProperty( property ), 
								model.createResource( object ) 
							 );
			
			
			return stmt;
		}
		
		private Statement createStatement(String subject, String property, String object) {
			
			stmt = model.createStatement
							 ( 	
								model.createResource( subject ), 
								model.createProperty( property ), 
								object 
							 );
			return stmt;
		}
		
		public void remove(Resource resource)
		{
			
			
			try
			{
				 	
				model.removeAll(resource, null, (RDFNode) null);
				
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		
		protected void removeUri(String subject, String property, String object )
		{
			
			
			try
			{
				 	
				Statement stmt = createStatement(subject, property, object);
						
				model.remove( stmt );
				 
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}

		
		protected List<Statement> get( String subject, String property, String object )
		{
			List<Statement> results = new ArrayList<Statement>();
			try
			{
				 
					
				Selector selector = new SimpleSelector(
							 model.createResource( subject ), 
							model.createProperty( property ),
							model.createResource( object ) 
							);
					
				StmtIterator it = model.listStatements( selector );
				{
					while( it.hasNext() )
					{
						Statement stmt = it.next(); 
						results.add( stmt );
					}
				}
					
				 
			} catch (Exception e) {
				e.printStackTrace();
			}
				
			return results;
		}
		
		protected ResultSet query(String sparql, String bd)
		{
			
			ResultSet results = null;
			QueryExecution qexec = null;
			
			try
			{				 	 
			     Query query = QueryFactory.create(sparql) ;			     
			     qexec = QueryExecutionFactory.sparqlService(bd, query);			     
			     		     
			     results = qexec.execSelect();		     
				 
			}  
			catch (Exception e) {
				e.printStackTrace();
			} 
			
			return results;
			
		}
		
		protected ResultSet query(String sparql)
		{
			
			ResultSet results = null;
			QueryExecution qexec = null;
			
			try
			{				 	 
			     Query query = QueryFactory.create(sparql) ;			     
			     qexec = QueryExecutionFactory.create(query, model) ;	
			     results = qexec.execSelect() ;		
			     				 
			} catch (Exception e) {	
				e.printStackTrace();
			} 
			
			return results;
			
		}
		
		protected ResultSet queryRDF(String sparql, Model model)
		{
			
			ResultSet results = null;
			QueryExecution qexec = null;
			
			try
			{				 	 
			     Query query = QueryFactory.create(sparql) ;			     
			     qexec = QueryExecutionFactory.create(query, model) ;	
			     results = qexec.execSelect() ;			     
				 
			} catch (Exception e) {
				e.printStackTrace();
			} 
			
			return results;
			
		}
		
		
		protected Resource getResource(String sparql, String var)
		{
			
			ResultSet results = null;
			QueryExecution qexec = null;
			QuerySolution qs = null;
			
			try
			{
				 	
				
			     
			     Query query = QueryFactory.create(sparql) ;
			     qexec = QueryExecutionFactory.create(query) ;		     
			     results = qexec.execSelect() ;
			     
			     qs = results.nextSolution();
			     		     
				 
			} catch (Exception e) {
				e.printStackTrace();
			}   
			
			return qs.getResource(var);
		}

		
	   protected Resource createResource(String uriResource, String uriRDFType) {
		   
		   Resource r = model.createResource(uriResource); 
		   
		   model.add(r, 
					RDF.type, 
					model.createResource(uriRDFType));
		   
		   return r;
		
	   }
	   
	   protected Resource createBN(String uriRDFType) {
		  
		   Resource bn = model.createResource(); 
		   
		  model.add(bn, 
					RDF.type, 
					model.createResource(uriRDFType)
					);
		  
		  return bn;
		
	   }
	
	
}
