package br.ufrj.macae.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.jena.query.Dataset;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ReadWrite;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.rdf.model.Selector;
import org.apache.jena.rdf.model.SimpleSelector;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.tdb.TDBFactory;
import org.apache.jena.vocabulary.RDF;

public class GenericDAO2 {
	
	protected final static Dataset dataSet = TDBFactory.createDataset( "db/colecaoDBTest1" );
	protected final static Model model = dataSet.getDefaultModel(); 
	
	private Statement stmt; 
	
	
	protected void addUri( String subject, String property, String object )
	{
		
		
		try
		{
			dataSet.begin( ReadWrite.WRITE );	
			
			stmt = createStatementUri(subject, property, object);
			
			model.add( stmt );
			dataSet.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dataSet.end();
		}
		
	}
	
	protected void add( String subject, String property, String object )
	{
		
		
		try
		{
			dataSet.begin( ReadWrite.WRITE );	
			
			stmt = createStatement(subject, property, object);
			
			model.add( stmt );
			dataSet.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dataSet.end();
		}
		
	}
	
	protected void addBlankNode( String subject, String property, Resource bn )
	{	
		try
		{
			dataSet.begin( ReadWrite.WRITE );	
			
			model.createResource(subject)
		    .addProperty( model.createProperty(property), 
		                 bn
		                );
			
			//model.add( stmt );
			dataSet.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dataSet.end();
		}
		
	}
	
	protected void addPropertyBlankNode( Resource bn,String property, String value )
	{	
		try
		{
			dataSet.begin( ReadWrite.WRITE );	
			
			
		    bn.addProperty( model.createProperty(property), 
		                  value
		                );
			
			//model.add( stmt );
			dataSet.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dataSet.end();
		}
		
	}
	
	protected void addResourceBlankNode( Resource bn,String property, Resource rBn )
	{	
		try
		{
			dataSet.begin( ReadWrite.WRITE );	
			
			
		    bn.addProperty( model.createProperty(property), 
		                  rBn
		                );
			
			dataSet.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dataSet.end();
		}
		
	}
	
	protected void addResourceExternalBlankNode( Resource bn,String property, String uriExternal )
	{	
		try
		{
			dataSet.begin( ReadWrite.WRITE );	
			
			
		    bn.addProperty( model.createProperty(property), 
		                  model.createResource(uriExternal)
		                );
			
			dataSet.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dataSet.end();
		}
		
	}
	
	protected void add( Resource resource, Property property, String value )
	{	
		try
		{
			dataSet.begin( ReadWrite.WRITE );	
			
			
			model.add(resource, property, value);
			
			
			dataSet.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dataSet.end();
		}
		
	}
	
	private Statement createStatementUri(String subject, String property, String object) {
					
		stmt = model.createStatement
						 ( 	
							model.createResource( subject ), 
							model.createProperty( property ), 
							model.createResource( object ) 
						 );
		
		
		return stmt;
	}
	
	private Statement createStatement(String subject, String property, String object) {
		
		stmt = model.createStatement
						 ( 	
							model.createResource( subject ), 
							model.createProperty( property ), 
							object 
						 );
		return stmt;
	}
	
	protected void remove(String subject, String property, String object )
	{
		
		
		try
		{
			dataSet.begin( ReadWrite.WRITE );	
			
			Statement stmt = createStatementUri(subject, property, object);
					
			model.remove( stmt );
			dataSet.commit();
		}
		finally
		{
			dataSet.end();
		}
	}
	
	protected void removeUri(String subject, String property, String object )
	{
		
		
		try
		{
			dataSet.begin( ReadWrite.WRITE );	
			
			Statement stmt = createStatement(subject, property, object);
					
			model.remove( stmt );
			dataSet.commit();
		}
		finally
		{
			dataSet.end();
		}
	}

	
	protected List<Statement> get( String subject, String property, String object )
	{
		List<Statement> results = new ArrayList<Statement>();
		try
		{
			dataSet.begin( ReadWrite.WRITE );
				
			Selector selector = new SimpleSelector(
						 model.createResource( subject ), 
						model.createProperty( property ),
						model.createResource( object ) 
						);
				
			StmtIterator it = model.listStatements( selector );
			{
				while( it.hasNext() )
				{
					Statement stmt = it.next(); 
					results.add( stmt );
				}
			}
				
			dataSet.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			dataSet.end();
		}
			
		return results;
	}
	
	protected ResultSet query(String sparql)
	{
		
		ResultSet results = null;
		QueryExecution qexec = null;
		
		try
		{
			dataSet.begin( ReadWrite.WRITE );	
			
		     
		     Query query = QueryFactory.create(sparql) ;
		     qexec = QueryExecutionFactory.create(query, dataSet) ;		     
		     results = qexec.execSelect() ;
		     
		     
			dataSet.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			qexec.close() ;
			dataSet.end();
		}
		
		return results;
		
	}
	
	protected Resource getResource(String sparql, String var)
	{
		
		ResultSet results = null;
		QueryExecution qexec = null;
		QuerySolution qs = null;
		
		try
		{
			dataSet.begin( ReadWrite.WRITE );	
			
		     
		     Query query = QueryFactory.create(sparql) ;
		     qexec = QueryExecutionFactory.create(query, dataSet) ;		     
		     results = qexec.execSelect() ;
		     
		     qs = results.nextSolution();
		     		     
			dataSet.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			qexec.close() ;
			dataSet.end();
		}
		
		return qs.getResource(var);
	}

	
   protected void rdfTypeResource(String uriResource, String uriRDFType) {
	   
	   model.add(ResourceFactory.createResource(uriResource), 
				RDF.type, 
				ResourceFactory.createResource(uriRDFType));
	
   }
   
   protected void rdfTypeResourceBN(Resource r, String uriRDFType) {
	   
	   model.add(r, 
				RDF.type, 
				ResourceFactory.createResource(uriRDFType));
	
   }

	
}
