package br.ufrj.macae.dao;

import java.io.Serializable;

import org.apache.jena.rdf.model.Resource;

import br.ufrj.macae.constante.DarwinCoreTerms;
import br.ufrj.macae.entity.Location;

public class LocationDAO extends GenericDAO implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4265895863876287676L;

	/*
	 * Primeiro cadastrar a ocorrencia, depois o taxon e os dados geográficos, por ultimo 
	 * a coleta.
	 * 	 */
	public void save(Location location) {
		
		Resource locationBN = createBN(DarwinCoreTerms.LOCATION);
		add(locationBN, DarwinCoreTerms.STATE_PROVIDENCE , location.getStateProvidence());
		add(locationBN , DarwinCoreTerms.MUNICIPALITY, location.getMunicipality());
		//add(locationBN , DarwinCoreTerms.COUNTY, location.getLocality());
		add(locationBN , DarwinCoreTerms.DECIMAL_LATITUDE, location.getDecimalLatitude().toString());
		add(locationBN , DarwinCoreTerms.DECIMAL_LONGITUDE, location.getDecimalLongitude().toString());
		//add(locationBN , DarwinCoreTerms.LOCALITY, location.getLocality());
		add(locationBN , DarwinCoreTerms.IN_DESCRIBED_PLACE, location.getInDescribedPlace());
		
		//location.setLocationBN(locationBN);
		
	}
	

}
