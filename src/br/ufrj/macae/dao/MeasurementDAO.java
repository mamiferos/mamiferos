package br.ufrj.macae.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.DCTerms;

import br.ufrj.macae.constante.DarwinCoreTerms;
import br.ufrj.macae.entity.MeasurementOrFact;
import br.ufrj.macae.entity.Occurrence;

public class MeasurementDAO extends GenericDAO {

	public void save(MeasurementOrFact measurement) {
		
		try {
		
		
		Resource measurementBN = createBN(DarwinCoreTerms.MEASUREMENT_OR_FACT);
		
		add(measurementBN, DarwinCoreTerms.MEASUREMENT_TYPE, measurement.getMeasurementType());
		add(measurementBN, DarwinCoreTerms.MEASUREMENT_VALUE, measurement.getMeasurementValue().toString());
		add(measurementBN, DarwinCoreTerms.MEASUREMENT_UNIT, measurement.getMeasurementUnit());
		
		//Relações 
		model.add(measurementBN, DCTerms.relation, measurement.getOccurrence());
		
		} catch (Exception e) {
			e.printStackTrace();
			
		}
				
	}
	
public List<MeasurementOrFact> list(Occurrence o) {	
	
		List<MeasurementOrFact> measurements = new ArrayList<MeasurementOrFact>();  
		
		try {
			
		StringBuffer sparql = new StringBuffer();

		sparql.append(" prefix dwc: <http://rs.tdwg.org/dwc/terms/> ");
		sparql.append(" prefix dsw: <http://purl.org/dsw/> ");
		sparql.append(" prefix dcterms: <http://purl.org/dc/terms/> ");
		sparql.append(" prefix dwciri: 	<http://rs.tdwg.org/dwc/iri/> ");		
		sparql.append(" PREFIX dbo:<http://dbpedia.org/ontology/> ");
		sparql.append(" PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> ");
		sparql.append(" PREFIX dbp: <http://dbpedia.org/property/>  ");	
				
		sparql.append(" SELECT * ");
		sparql.append(" WHERE { " );		
		sparql.append(" ?measurement a dwc:MeasurementOrFact . ");	
		sparql.append(" ?measurement dcterms:relation ?occurrence ; ");
		sparql.append(" dwc:measurementType ?measurementType ; ");
		sparql.append(" dwc:measurementValue ?measurementValue ; ");
		sparql.append(" dwc:measurementUnit ?measurementUnit ; ");
		
				
		sparql.append(" FILTER(?occurrence = <" + o.getUri() + ">)");
		
		sparql.append("}");
		
				
		ResultSet rs = query(sparql.toString());
		MeasurementOrFact m;
		while(rs.hasNext()) {
						
			QuerySolution solution = rs.nextSolution();
				
			m = new MeasurementOrFact();
			m.setMeasurementType(solution.getLiteral("measurementType").getString());
			m.setMeasurementValue(solution.getLiteral("measurementValue").getString());
			m.setMeasurementUnit(solution.getLiteral("measurementUnit").getString());
			
			measurements.add(m);
						
		}
            
		
		} catch (Exception ex) {
			// TODO: handle exception
			ex.printStackTrace();
		}
		
		return measurements;

	}

	
}
