package br.ufrj.macae.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.DCTerms;
import org.apache.jena.vocabulary.RDF;

import br.ufrj.macae.constante.DarwinCoreTerms;
import br.ufrj.macae.constante.NameSpace;
import br.ufrj.macae.entity.Identification;
import br.ufrj.macae.entity.Location;
import br.ufrj.macae.entity.Occurrence;

public class OccurrenceDAO extends GenericDAO {

	public Resource insert(Occurrence occurrence) {
		
		Resource occurrenceRS = null;
		
		try {			
			
					
		String uri = NameSpace.URI_BASE + occurrence.getCatalogNumber();
		
		//Occurence
		occurrenceRS = model.createResource(uri);
		
		model.add(occurrenceRS, 
				RDF.type, 
				model.createResource(DarwinCoreTerms.OCCURRENCE) );
		
		add(occurrenceRS, DarwinCoreTerms.CATALOG_NUMBER, occurrence.getCatalogNumber());
		add(occurrenceRS, DarwinCoreTerms.RECORD_NUMBER, occurrence.getRecordNumber());		
		add(occurrenceRS, DarwinCoreTerms.OCCURRENCE_REMARKS, occurrence.getOccurrenceRemarks());
		add(occurrenceRS, DarwinCoreTerms.SEX, occurrence.getSex());
		add(occurrenceRS, DarwinCoreTerms.LOCALITY, occurrence.getLocality()); //estação de coleta
		add(occurrenceRS, DarwinCoreTerms.PREPARATIONS, occurrence.getPreparations());
		add(occurrenceRS, DarwinCoreTerms.DISPOSITION, occurrence.getDisposition());
		
		//Relações 
		add(occurrenceRS, DarwinCoreTerms.AT_EVENT, model.createResource(occurrence.getEvent().getUri()));
		model.add(occurrenceRS, DCTerms.relation, occurrence.getIdentification());
						
		
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		
		return occurrenceRS;
				
	}
	
	public boolean searchTombo(String tombo) {
		
		
		
		try {
			
		StringBuffer sparql = new StringBuffer();

		sparql.append(" prefix dwc: <http://rs.tdwg.org/dwc/terms/> ");
		
		sparql.append(" SELECT distinct ");
		sparql.append(" ?tombo ");		
		sparql.append( " WHERE { ?occurrence a dwc:Occurrence ; ");		
		sparql.append(" dwc:catalogNumber  ?tombo. ");		
		sparql.append(" filter regex(str(?tombo),'" + tombo + "', 'i' ) .  } LIMIT 1 ");

		
		ResultSet rs = query(sparql.toString());

		if (rs.hasNext()) {
			return true;
		} 
		
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return false;

	}

	
	public  void getIdentification(Occurrence occurrence) {

		
		try {
			StringBuffer sparql = new StringBuffer();
	
			sparql.append(" prefix dwc: <http://rs.tdwg.org/dwc/terms/> ");
			sparql.append(" prefix dsw: <http://purl.org/dsw/> ");
			sparql.append(" prefix dcterms: <http://purl.org/dc/terms/> ");
					
			sparql.append(" SELECT * ");
			sparql.append(" WHERE { " );		
			sparql.append(" ?occurrence a dwc:Occurrence; ");		
			sparql.append(" dsw:atEvent ?event . ");		
			sparql.append(" ?occurrence dcterms:relation ?identification . ");
			 
			
			sparql.append(" FILTER(?event = <" + occurrence.getEvent().getUri() + ">)");
			
			sparql.append("}");
	
			
			ResultSet rs = query(sparql.toString());
	
			Identification i;
			Location l;
			
			QuerySolution solution = rs.nextSolution();
				
			occurrence.setIdentification( solution.getResource("identification") );
			occurrence.setOccurrence( solution.getResource("occurrence") );
            
		
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}


	}

	
public List<Occurrence> view(Occurrence occurrence) {	
	
	List<Occurrence> occurrences = new ArrayList<Occurrence>();
	
		try {
		StringBuffer sparql = new StringBuffer();

		sparql.append(" prefix dwc: <http://rs.tdwg.org/dwc/terms/> ");
		sparql.append(" prefix dsw: <http://purl.org/dsw/> ");
		sparql.append(" prefix dcterms: <http://purl.org/dc/terms/> ");
		sparql.append(" prefix dwciri: 	<http://rs.tdwg.org/dwc/iri/> ");		
		sparql.append(" PREFIX dbo:<http://dbpedia.org/ontology/> ");
		sparql.append(" PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> ");
		sparql.append(" PREFIX dbp: <http://dbpedia.org/property/>  ");	
				
		sparql.append(" SELECT * ");
		sparql.append(" WHERE { " );		
		sparql.append(" ?occurrence a dwc:Occurrence; ");		
		sparql.append(" dsw:atEvent ?event . ");
		sparql.append(" ?occurrence dwc:catalogNumber ?catalogNumber ; ");
		sparql.append(" dwc:recordNumber ?recordNumber ; ");
		sparql.append(" dwc:occurrenceRemarks ?occurrenceRemarks ; ");
		sparql.append(" dwc:sex ?sex ; ");
		sparql.append(" dwc:preparations  ?preparations ; ");
		sparql.append(" dwc:locality ?samplingStation ; ");
		sparql.append(" dwc:disposition  ?disposition . ");
				
		sparql.append(" FILTER(?event = <" + occurrence.getEvent().getUri() + ">)");
		
		sparql.append("}");
		
				
		ResultSet rs = query(sparql.toString());

		Occurrence o;
		while(rs.hasNext()) {
						
			QuerySolution solution = rs.nextSolution();
			o = new Occurrence();
			
			o.setUri(solution.getResource("occurrence").getURI());
			o.setCatalogNumber(solution.getLiteral("catalogNumber").getString());
			o.setRecordNumber(solution.getLiteral("recordNumber").getString());
			o.setOccurrenceRemarks(solution.getLiteral("occurrenceRemarks").getString());
			o.setSex(solution.getLiteral("sex").getString());
			o.setPreparations(solution.getLiteral("preparations").getString());
			o.setLocality(solution.getLiteral("samplingStation").getString()); //estação de coleta
			o.setDisposition(solution.getLiteral("disposition").getString());
			
			occurrences.add(o);
		}
            
		
		} catch (Exception ex) {
			// TODO: handle exception
			ex.printStackTrace();
		}
		
		return occurrences;

	}
	
	
	
}
