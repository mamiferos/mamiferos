package br.ufrj.macae.dao;

import org.apache.jena.atlas.io.IndentedWriter;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.sparql.vocabulary.FOAF;
import org.apache.jena.vocabulary.DC;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.VCARD;

import br.ufrj.macae.entity.Researcher;

public class PersonDAO extends GenericDAO {

	public void insert(Researcher person) {
		
		try {
			
			

			//person
			Resource personBN = model.createResource();
			
			model.add(personBN, 
					RDF.type, 
					FOAF.Person);
			
			/*
			add(personBN, FOAF.name, person.getName());
			add(personBN,FOAF.firstName, person.getFirstName());
			add(personBN,FOAF.family_name, person.getFamilyName());
			add(personBN,DC.identifier, person.getCpf());
			add(personBN,VCARD.KEY, person.getKeyWord());
			*/
			
			
			select(model);
			model.write(System.out, "TURTLE");
			
			
		} catch (Exception e) {
			e.printStackTrace();
			
		}
				
	}
	
	public static void select(Model model)
    {
        // Create the data.
        // This wil be the background (unnamed) graph in the dataset.
       // Model model = createModel() ;
        
        // First part or the query string 
        String prolog = "PREFIX foaf: <"+FOAF.getURI()+">" ;
        
        // Query string.
        String queryString = 
            "SELECT * WHERE {?s ?p ?o}" ; 
        
        Query query = QueryFactory.create(queryString) ;
        // Print with line numbers
        query.serialize(new IndentedWriter(System.out,true)) ;
        System.out.println() ;
        
        // Create a single execution of this query, apply to a model
        // which is wrapped up as a Dataset
        
        try(QueryExecution qexec = QueryExecutionFactory.create(query, model)){
            // Or QueryExecutionFactory.create(queryString, model) ;


            // Assumption: it's a SELECT query.
            ResultSet rs = qexec.execSelect() ;
            
            ResultSetFormatter.out(System.out, rs, query) ;	
         
        }
    }
	
	
}
