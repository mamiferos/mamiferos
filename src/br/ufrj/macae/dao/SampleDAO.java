package br.ufrj.macae.dao;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.context.FacesContext;

import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.DCTerms;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;

import br.ufrj.macae.constante.DarwinCoreTerms;
import br.ufrj.macae.constante.DataSets;
import br.ufrj.macae.entity.Event;
import br.ufrj.macae.entity.Identification;
import br.ufrj.macae.entity.Location;
import br.ufrj.macae.entity.Recorder;
import br.ufrj.macae.entity.SamplingProtocol;
import br.ufrj.macae.tic.util.Util;

public class SampleDAO extends GenericDAO implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 4265895863876287676L;
	
	private int error;

	public void save(Event event) throws IOException {
		
		//event
		String uri = Util.gerarUri();
		event.setUri(uri);
		
		
		FacesContext.getCurrentInstance().getExternalContext().redirect("insertSpecimen.jsf?uri=" + uri );
		
		
		Resource eventRS = model.createResource(uri);
		model.add(eventRS, 
				RDF.type, 
				model.createResource(DarwinCoreTerms.EVENT));

		String samplingPrococol = "";
		int cont= 0;
		for (SamplingProtocol samplingProtocol : event.getSamplingProtocol()) {
			if(cont == 0) {
				samplingPrococol = samplingProtocol.getSamplingProtocol();
			} else {
				samplingPrococol = samplingPrococol + " - " + samplingProtocol.getSamplingProtocol();
			}
			
			cont++;
			
		}
		add(eventRS, DarwinCoreTerms.SAMPLING_PROTOCOL, samplingPrococol);	
	
		add(eventRS, DarwinCoreTerms.RECORDED_BY, event.getRecordedBy());		
		
		String remarks = event.getEventRemarks(); // observação sobre o local da coleta
		add(eventRS, DarwinCoreTerms.EVENT_DATE,Util.formatarDataIso8601(event.getEventDate()), XSDDatatype.XSDdate);	

		add(eventRS, DarwinCoreTerms.EVENT_REMARKS, remarks);		
		
		// obs sobre o local da coleta, foi colocado aqui, pois esse dado é associado a coleta. 
		// O local pode esta relacionado com outras coletas.
		
		if(event.getFieldNotes() == null) {
			add(eventRS, DarwinCoreTerms.FIELD_NOTES, "");
		} else {
			add(eventRS, DarwinCoreTerms.FIELD_NOTES, event.getFieldNotes());
		}
		

		Identification identification = event.getIdentification();
		Resource  identificationBN;
		
		identificationBN = getIdentificationBN(identification); 
			
		if(identificationBN == null) {
		
			identificationBN = model.createResource();
			
			model.add(identificationBN, 
					RDF.type, 
					model.createResource(DarwinCoreTerms.IDENTIFICATION));
			
			//add(identificationBN, DarwinCoreTerms.SCIENTIFIC_NAME, identification.getScientificName());
			//add(identificationBN, DarwinCoreTerms.FAMILY, identification.getFamily());
			//add(identificationBN, DarwinCoreTerms.ORDER, identification.getOrder());
			
			//add(identificationBN, RDFS.comment, identification.getComment());
			add(identificationBN, RDFS.seeAlso, model.createResource(identification.getSeeAlso())); 
			//add(identificationBN, FOAF.thumbnail, identification.getThumbnail());
			add(identificationBN, DarwinCoreTerms.TO_TAXON, model.createResource(identification.getToTaxon()) );
		
		}
		
		//Location

		//Resource locationBN = getLocality(event.getLocation());
		
		//if(locationBN == null) {
		Resource locationBN = model.createResource();

			model.add(locationBN, 
					RDF.type, 
					DCTerms.Location);

			Location location = event.getLocation();

			//add(locationBN, DarwinCoreTerms.STATE_PROVIDENCE, location.getStateProvidence());
			//add(locationBN, DarwinCoreTerms.COUNTY, location.getCounty());//município
			add(locationBN, DarwinCoreTerms.MUNICIPALITY, location.getMunicipality());//bairro		
			add(locationBN, DarwinCoreTerms.IN_DESCRIBED_PLACE, model.createResource(location.getInDescribedPlace()) );
					
			add(locationBN, DarwinCoreTerms.DECIMAL_LATITUDE, location.getDecimalLatitude().toString());
			add(locationBN, DarwinCoreTerms.DECIMAL_LONGITUDE, location.getDecimalLongitude().toString());
			add(locationBN, DarwinCoreTerms.LOCATION_REMARKS, location.getLocationRemarks());

		//}
						
		add(eventRS, DarwinCoreTerms.LOCATE_AT, locationBN);


		//Occurence
		Resource occurrenceBN = model.createResource();
	

		model.add(occurrenceBN, 
				RDF.type, 
				model.createResource(DarwinCoreTerms.OCCURRENCE));


		//Relações 
		add(occurrenceBN, DarwinCoreTerms.AT_EVENT, eventRS);
		model.add(occurrenceBN, DCTerms.relation, identificationBN);

	}

	public List<String> searchSpecies(String query) {
		
		// usar esse metodo searchSpecieDBPedia

		List<String> species = new ArrayList<String>();
		
		try {
		StringBuffer sparql = new StringBuffer();

		sparql.append(" prefix dwc: <http://rs.tdwg.org/dwc/terms/> ");
		
		sparql.append(" SELECT distinct ");
		sparql.append(" ?specie ");		
		sparql.append( " WHERE { ?identification a dwc:Identification ; ");		
		sparql.append(" dwc:scientificName  ?specie . ");		
		sparql.append(" filter regex(str(?specie),'" + query + "', 'i' ) .  } LIMIT 6 ");

		
		ResultSet rs = query(sparql.toString());

		while (rs.hasNext()) {
			QuerySolution solution = rs.nextSolution();
			RDFNode sp = solution.get("specie") ;
            Literal specieStr = (Literal) sp;

			species.add(specieStr.getString());
		}
		
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return species;

	}
	
	
public List<String> searchSpecieDBPedia(String query, List<Identification> identificationList) {
		

		Identification identification = new Identification();
		List<String> species = new ArrayList<String>();
		StringBuffer sparql = new StringBuffer();
		
		sparql.append(" PREFIX dbo:<http://dbpedia.org/ontology/> ");
		sparql.append(" PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> ");
		sparql.append(" PREFIX dbp: <http://dbpedia.org/property/>  ");		
		
		sparql.append(" SELECT distinct ");
		sparql.append("  * ");		
		sparql.append( " WHERE { ?mammal a dbo:Mammal . ");		
		sparql.append( " ?mammal dbp:binomial ?specie . ");
		sparql.append( " ?mammal dbo:thumbnail ?thumbnail . ");
		//sparql.append( " ?mammal dbo:order ?order. ");
		//sparql.append( " ?mammal dbo:family ?family . ");
		sparql.append( " ?family rdfs:label ?familyName . ");
		sparql.append( " ?order rdfs:label ?orderName ");
		
		//sparql.append( " FILTER ( lang(?specie) = 'pt' )" );
		sparql.append( " FILTER ( lang(?familyName) = 'pt' )" );
		sparql.append( " FILTER ( lang(?orderName) = 'pt' )  "  );
		sparql.append(" filter regex(str(?specie),'" + query + "', 'i' ) .  } LIMIT 3 ");
		
		ResultSet rs = query(sparql.toString(), DataSets.DB_PEDIA);
		
		while (rs.hasNext()) {
			QuerySolution solution = rs.nextSolution();
			
			identification.setScientificName( solution.getLiteral("specie").getString() );
			identification.setOrder( solution.getLiteral("orderName").getString() );
			identification.setFamily ( solution.getLiteral("familyName").getString() );	
			identification.setThumbnail(solution.getResource("thumbnail").getURI());
			
			identificationList.add(identification);
			species.add(identification.getScientificName());
		}
		
		return species;
	}
	

	public void searchSpecie(Identification identification) {

		StringBuffer sparql = new StringBuffer();

		sparql.append(" prefix dwc: <http://rs.tdwg.org/dwc/terms/> ");
		sparql.append(" SELECT distinct ");
		sparql.append(" ?specie ?order ?family ?identification ");		
		sparql.append( " WHERE { ?identification a dwc:Identification ; ");		
		sparql.append(" dwc:scientificName  ?specie; ");
		sparql.append(" dwc:order  ?order; ");
		sparql.append(" dwc:family  ?family . ");
		sparql.append(" filter regex(str(?specie),'" + identification.getScientificName() + "', 'i' ) .  } LIMIT 1");

		ResultSet rs = query(sparql.toString());	

		rs.hasNext();
		QuerySolution solution = rs.nextSolution();
		RDFNode sp = solution.get("identification") ;
        Resource r = (Resource) sp;				
		identification.setId(r.getId().toString());
		
		sp = solution.get("specie") ;
        Literal l = (Literal) sp;
		identification.setScientificName(l.getString());
		
		sp = solution.get("order") ;
        l = (Literal) sp;
		identification.setOrder(l.getString());
		
		sp = solution.get("family") ;
        l = (Literal) sp;
		identification.setFamily(l.getString());

	}
	
	public Resource getIdentificationBN(Identification identification) {

		StringBuffer sparql = new StringBuffer();

		sparql.append(" prefix dwc: <http://rs.tdwg.org/dwc/terms/> ");
		sparql.append(" prefix dwciri: 	<http://rs.tdwg.org/dwc/iri/> ");		
		
		sparql.append(" SELECT distinct ");
		sparql.append(" ?identification ?toTaxon ");		
		sparql.append( " WHERE { ?identification a dwc:Identification ; ");		
		sparql.append(" dwciri:toTaxon  ?toTaxon . ");
		sparql.append(" filter (?toTaxon = <" + identification.getToTaxon() + ">) .  } LIMIT 1");

		ResultSet rs = query(sparql.toString());	

		if(rs.hasNext()) {
			QuerySolution solution = rs.nextSolution();
			return solution.getResource("identification"); 
		}
		
		
		return null;       				
		
	}
	
	public void searchLocation(Location location) {

		
		try {
			StringBuffer sparql = new StringBuffer();
	
			sparql.append(" prefix dwc: <http://rs.tdwg.org/dwc/terms/> ");
			
			sparql.append(" SELECT distinct ?stateProvidence ?municipality ?locality ?decimalLongitude ?decimalLatitude ?location ");		
			sparql.append( " WHERE { ?location a dcterms:Location; ");		
			sparql.append(" dwc:stateProvidence  ?stateProvidence . ");
			sparql.append(" dwc:municipality  ?municipality . ");
			sparql.append(" dwc:locality  ?locality . ");
			
			sparql.append(" filter regex(str(?decimalLatitude ),'" + location.getDecimalLatitude() + "', 'i' ) ");
			sparql.append(" filter regex(str(?decimalLongitude ),'" + location.getDecimalLongitude() + "', 'i' ) .  } LIMIT 6 ");
	
			
			ResultSet rs = query(sparql.toString());
	
			rs.hasNext() ; 
			QuerySolution solution = rs.nextSolution();
	        
	        location.setStateProvidence(solution.getLiteral("stateProvidence").getString());	        
	        location.setMunicipality(solution.getLiteral("municipality").getString());	        
	        
	        //location.setLocality(solution.getLiteral("locality").getString());
	        
	        location.setLocationBN(solution.getResource("location"));
		
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}
	
	
	public List<Event> list() {

		List<Event> samples = new ArrayList<Event>();
		
		try {
		StringBuffer sparql = new StringBuffer();

		sparql.append(" prefix dwc: <http://rs.tdwg.org/dwc/terms/> ");
		sparql.append(" prefix dsw: <http://purl.org/dsw/> ");
		sparql.append(" prefix dcterms: <http://purl.org/dc/terms/> ");
		sparql.append(" prefix dwciri: 	<http://rs.tdwg.org/dwc/iri/> ");
				
		sparql.append(" SELECT distinct ?event ?identification ?eventDate ?toTaxon ");
		sparql.append(" WHERE { " );		
		sparql.append(" ?occurrence a dwc:Occurrence; ");		
		sparql.append(" dsw:atEvent ?event . ");
		
		sparql.append(" ?event dwc:eventDate ?eventDate . ");
		sparql.append(" ?occurrence dcterms:relation ?identification . ");
		sparql.append(" ?identification dwciri:toTaxon ?toTaxon . ");	
				
		sparql.append("} ORDER BY desc(?eventDate) LIMIT 6 ");

		
		ResultSet rs = query(sparql.toString());

		Event e;
		Identification i;
		String date; 
		//int index;
		//String comment;
		while (rs.hasNext()) {
			QuerySolution solution = rs.nextSolution();
			
			e = new Event();
			i =  new Identification();
			
			i.setToTaxon( solution.getResource("toTaxon").getURI());
			i.setId( solution.getResource("identification").getURI());
			searchSpecieDBPedia(i);	
			
			e.setUri(solution.getResource("event").getURI());
			
			date = solution.getLiteral("eventDate").getValue().toString();			
			date = date.replaceAll("-", "/");
			e.setEventDate(new Date(date));
						
			//index = i.getComment().indexOf(".");
			//comment = i.getComment().substring(0, index);
			//i.setComment(comment);
			
			e.setIdentification(i);
            
			samples.add(e);
		}
		
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return samples;

	}
	
	public List<Event> list(int page) {


		List<Event> samples = new ArrayList<Event>();
		
		try {
		StringBuffer sparql = new StringBuffer();

		sparql.append(" prefix dwc: <http://rs.tdwg.org/dwc/terms/> ");
		sparql.append(" prefix dsw: <http://purl.org/dsw/> ");
		sparql.append(" prefix dcterms: <http://purl.org/dc/terms/> ");
		sparql.append(" prefix dwciri: 	<http://rs.tdwg.org/dwc/iri/> ");
				
		sparql.append(" SELECT distinct ?event ?identification ?toTaxon ?eventDate ");
		sparql.append(" WHERE { " );		
		sparql.append(" ?occurrence a dwc:Occurrence; ");		
		sparql.append(" dsw:atEvent ?event . ");
		
		sparql.append(" ?event dwc:eventDate ?eventDate . ");
		sparql.append(" ?occurrence dcterms:relation ?identification . ");
		sparql.append(" ?identification dwciri:toTaxon ?toTaxon . ");	
				
		sparql.append("} ORDER BY desc(?eventDate) LIMIT 6 " + " OFFSET " + (page) * 6 );

		
		ResultSet rs = query(sparql.toString());

		Event e;
		Identification i;
		String date;
		//int index;
		//String comment;
		while (rs.hasNext()) {
			QuerySolution solution = rs.nextSolution();
			
			e = new Event();
			i =  new Identification();
			
			i.setToTaxon( solution.getResource("toTaxon").getURI());
			i.setId( solution.getResource("identification").getURI());
			searchSpecieDBPedia(i);	
			
			e.setUri(solution.getResource("event").getURI());
			
			
			date = solution.getLiteral("eventDate").getValue().toString();			
			date = date.replaceAll("-", "/");
			e.setEventDate(new Date(date));
			
			//index = i.getComment().indexOf(".");
			//comment = i.getComment().substring(0, index);
			//i.setComment(comment);
			
			e.setIdentification(i);
            
			samples.add(e);
		}
		
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return samples;

	}
	
	public List<Event> searchForPeriod(String inicio, String fim) {

		List<Event> samples = new ArrayList<Event>();
		
		try {
			
		StringBuffer sparql = new StringBuffer();

		sparql.append(" prefix dwc: <http://rs.tdwg.org/dwc/terms/> ");
		sparql.append(" prefix dsw: <http://purl.org/dsw/> ");
		sparql.append(" prefix dcterms: <http://purl.org/dc/terms/> ");
		sparql.append(" prefix dwciri: 	<http://rs.tdwg.org/dwc/iri/> ");
		sparql.append(" prefix xsd: 	<http://www.w3.org/2001/XMLSchema#> ");
				
		sparql.append(" SELECT distinct ?event ?identification ?toTaxon ?eventDate ");
		sparql.append(" WHERE { " );		
		sparql.append(" ?occurrence a dwc:Occurrence; ");		
		sparql.append(" dsw:atEvent ?event . ");
		sparql.append(" ?event dwc:eventDate ?eventDate . ");
		
		sparql.append(" ?occurrence dcterms:relation ?identification . ");
		sparql.append(" ?identification dwciri:toTaxon ?toTaxon . ");	
		
		sparql.append(" FILTER(?eventDate >= '" + inicio + "'^^xsd:date)");
		sparql.append(" FILTER(?eventDate <= '" + fim + "'^^xsd:date)");
				
		sparql.append("} LIMIT 6 ");

		
		ResultSet rs = query(sparql.toString());

		Event e;
		Identification i;
		String date;
		//int index;
		//String comment;
		while (rs.hasNext()) {
			QuerySolution solution = rs.nextSolution();
			
			e = new Event();
			i =  new Identification();
			
			i.setToTaxon( solution.getResource("toTaxon").getURI());
			i.setId( solution.getResource("identification").getURI());
			searchSpecieDBPedia(i);	
			
			e.setUri(solution.getResource("event").getURI());
			
			date = solution.getLiteral("eventDate").getValue().toString();			
			date = date.replaceAll("-", "/");
			e.setEventDate(new Date(date));
			
			e.setIdentification(i);
            
			samples.add(e);
		}
		
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return samples;

	}
		
	public List<Event> searchForIdentification(Identification identification) {

		List<Event> samples = new ArrayList<Event>();
		
		try {
		StringBuffer sparql = new StringBuffer();

		sparql.append(" prefix dwc: <http://rs.tdwg.org/dwc/terms/> ");
		sparql.append(" prefix dsw: <http://purl.org/dsw/> ");
		sparql.append(" prefix dcterms: <http://purl.org/dc/terms/> ");
		sparql.append(" prefix dwciri: 	<http://rs.tdwg.org/dwc/iri/> ");
				
		sparql.append(" SELECT distinct ?event ?identification ?toTaxon ?eventDate ");
		sparql.append(" WHERE { " );		
		sparql.append(" ?occurrence a dwc:Occurrence; ");		
		sparql.append(" dsw:atEvent ?event . ");
		sparql.append(" ?event dwc:eventDate ?eventDate . ");
		
		sparql.append(" ?occurrence dcterms:relation ?identification . ");
		sparql.append(" ?identification dwciri:toTaxon ?toTaxon . ");	
		
		sparql.append(" filter (?toTaxon = <" + identification.getToTaxon() + ">) ");
				
		sparql.append("} LIMIT 6 ");

		
		ResultSet rs = query(sparql.toString());

		Event e;
		Identification i;
		String date;
		//int index;
		//String comment;
		while (rs.hasNext()) {
			QuerySolution solution = rs.nextSolution();
			
			e = new Event();
			i =  new Identification();
			
			i.setToTaxon( solution.getResource("toTaxon").getURI());
			i.setId( solution.getResource("identification").getURI());
			searchSpecieDBPedia(i);	
			
			e.setUri(solution.getResource("event").getURI());
			
			date = solution.getLiteral("eventDate").getValue().toString();			
			date = date.replaceAll("-", "/");
			e.setEventDate(new Date(date));
			
			e.setIdentification(i);
            
			samples.add(e);
		}
		
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return samples;

	}
			
	public List<Event> searchForMunicipality(Location location) {

		List<Event> samples = new ArrayList<Event>();
		
		try {
		StringBuffer sparql = new StringBuffer();

		sparql.append(" prefix dwc: <http://rs.tdwg.org/dwc/terms/> ");
		sparql.append(" prefix dsw: <http://purl.org/dsw/> ");
		sparql.append(" prefix dcterms: <http://purl.org/dc/terms/> ");
		sparql.append(" prefix dwciri: 	<http://rs.tdwg.org/dwc/iri/> ");
				
		sparql.append(" SELECT distinct ?event ?identification ?toTaxon ?eventDate ");
		sparql.append(" WHERE { " );		
		sparql.append(" ?occurrence a dwc:Occurrence; ");		
		sparql.append(" dsw:atEvent ?event . ");
		sparql.append(" ?event dwc:eventDate ?eventDate . ");
		
		sparql.append(" ?occurrence dcterms:relation ?identification . ");
		sparql.append(" ?identification dwciri:toTaxon ?toTaxon . ");	
		sparql.append(" ?event dsw:locateAt ?location . ");
		sparql.append(" ?location dwciri:inDescribedPlace ?inDescribedPlace . ");
		
		sparql.append(" filter (?inDescribedPlace = <" + location.getInDescribedPlace() + ">) ");
				
		sparql.append("} LIMIT 6 ");

		
		ResultSet rs = query(sparql.toString());

		Event e;
		Identification i;
		String date;
		//int index;
		//String comment;
		while (rs.hasNext()) {
			QuerySolution solution = rs.nextSolution();
			
			e = new Event();
			i =  new Identification();
			
			i.setToTaxon( solution.getResource("toTaxon").getURI());
			i.setId( solution.getResource("identification").getURI());
			searchSpecieDBPedia(i);	
			
			e.setUri(solution.getResource("event").getURI());
			
			date = solution.getLiteral("eventDate").getValue().toString();			
			date = date.replaceAll("-", "/");
			e.setEventDate(new Date(date));
			
			e.setIdentification(i);
            
			samples.add(e);
		}
		
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return samples;

	}


	
	public void view(Event e) {	
		
		try {
		StringBuffer sparql = new StringBuffer();

		sparql.append(" prefix dwc: <http://rs.tdwg.org/dwc/terms/> ");
		sparql.append(" prefix dsw: <http://purl.org/dsw/> ");
		sparql.append(" prefix dcterms: <http://purl.org/dc/terms/> ");
		sparql.append(" prefix dwciri: 	<http://rs.tdwg.org/dwc/iri/> ");		
		sparql.append(" PREFIX dbo:<http://dbpedia.org/ontology/> ");
		sparql.append(" PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> ");
		sparql.append(" PREFIX dbp: <http://dbpedia.org/property/>  ");	
				
		sparql.append(" SELECT * ");
		sparql.append(" WHERE { " );		
		sparql.append(" ?occurrence a dwc:Occurrence; ");		
		sparql.append(" dsw:atEvent ?event . ");
		sparql.append(" ?event dwc:eventDate ?data ; ");
		//sparql.append(" dwc:fieldNotes ?fieldNotes ; ");
		sparql.append(" dwc:eventRemarks ?eventRemarks ; ");
		sparql.append(" dwc:recordedBy ?recordedBy ; ");
		sparql.append(" dwc:samplingProtocol  ?samplingProtocol . ");
		
		sparql.append(" ?occurrence dcterms:relation ?identification . ");
		sparql.append(" ?identification dwciri:toTaxon ?toTaxon ; ");
		sparql.append(" rdfs:seeAlso ?seeAlso . ");
				
		
		sparql.append(" ?event dsw:locateAt ?location . ");		
		sparql.append(" ?location dwc:decimalLongitude ?longitude; ");
		sparql.append(" dwc:decimalLatitude ?latitude; ");
		sparql.append(" dwc:municipality ?municipality ; ");
		sparql.append(" dwc:locationRemarks ?locationRemarks ; ");
		sparql.append(" dwciri:inDescribedPlace ?inDescribedPlace . ");
				
		sparql.append(" FILTER(?event = <" + e.getUri() + ">)");
		
		sparql.append("} LIMIT 1 ");

		
		ResultSet rs = query(sparql.toString());

		Location l;
		
			QuerySolution solution = rs.nextSolution();
			
			e.getIdentification().setSeeAlso(solution.getResource("seeAlso").getURI());
			
			l = new Location();		
			
			l.setInDescribedPlace( solution.getResource("inDescribedPlace").getURI());	
			searchGeonames(l);
									
			String date = solution.getLiteral("data").getValue().toString();
			
			date = date.replaceAll("-", "/");
			e.setEventDate(new Date(date));
			e.setEventRemarks(solution.getLiteral("eventRemarks").getString());
			//e.setFieldNotes(solution.getLiteral("fieldNotes").getString());
			e.setRecordedBy(solution.getLiteral("recordedBy").getString());
						
			//e.setSamplingProtocol(getSamplingProtocol(solution));
			e.setProtocol(solution.getLiteral("samplingProtocol").getString());
			//e.setRecordedByList(getRecorder(solution));
			
			e.setLocation(l);
            
		
		} catch (Exception ex) {
			// TODO: handle exception
			ex.printStackTrace();
		}

	}

	private List<SamplingProtocol> getSamplingProtocol(QuerySolution solution) {
		
		boolean flag = true;
		List<SamplingProtocol> sl = new ArrayList<SamplingProtocol>();
		while(flag) {			
			
			SamplingProtocol s = new SamplingProtocol();
			String p = solution.getLiteral("samplingProtocol").getString();
			int aux = p.indexOf("-");
			if(aux > 0) {
				String v = p.substring(0,aux);
				s.setSamplingProtocol(v);				
			} else {
				s.setSamplingProtocol(p);
				flag = false;
			}
			sl.add(s);
		}
		
		return sl;
	}
	
	private List<Recorder> getRecorder(QuerySolution solution) {
		
		boolean flag = true;
		List<Recorder> rl = new ArrayList<Recorder>();
		while(flag) {			
			
			Recorder r = new Recorder();
			String p = solution.getLiteral("recordedBy").getString();
			int aux = p.indexOf("-");
			if(aux > 0) {
				String v = p.substring(0,aux);
				r.setName(v);				
			} else {
				r.setName(p);
				flag = false;
			}
			rl.add(r);
		}
		
		return rl;
	}
	
	public void searchSpecieDBPedia(Identification identification) {
		
		StringBuffer sparql = new StringBuffer();
		
		sparql.append(" PREFIX dbo:<http://dbpedia.org/ontology/> ");
		sparql.append(" PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> ");
		sparql.append(" PREFIX dbp: <http://dbpedia.org/property/>  ");		
		
		sparql.append(" SELECT distinct ");
		sparql.append("  * ");		
		sparql.append( " WHERE { ?mammal a dbo:Mammal . ");		
		sparql.append( " ?mammal dbp:binomial ?specie . ");
		//sparql.append( " ?mammal rdfs:label ?labelSpecie  . ");
		//sparql.append( " ?mammal dbo:order ?order. ");
		//sparql.append( " ?mammal dbo:family ?family . ");
		sparql.append( " OPTIONAL { ?mammal dbo:thumbnail ?thumbnail } . ");
		sparql.append( " ?mammal rdfs:comment ?comment .");
		//sparql.append( " ?family rdfs:label ?familyName . ");
		//sparql.append( " ?order rdfs:label ?orderName ");
		
		sparql.append(" FILTER ( lang(?comment) = 'pt' ) ");
		//sparql.append( " FILTER ( lang(?familyName) = 'pt' )" );
		//sparql.append( " FILTER ( lang(?orderName) = 'pt' )  "  );
		//sparql.append( " FILTER ( lang(?labelSpecie) = 'pt' )  "  );
		sparql.append(" FILTER (?mammal = <" + identification.getToTaxon() + ">) .  } ");
		
		ResultSet rs = query(sparql.toString(), DataSets.DB_PEDIA);
		
		if(rs.hasNext()) { 
			QuerySolution solution = rs.nextSolution();
			
			identification.setScientificName( solution.getLiteral("specie").getString() );
		
			
			//identification.setOrder( solution.getLiteral("orderName").getString() );
			//identification.setFamily ( solution.getLiteral("familyName").getString() );		
	
			identification.setComment(solution.getLiteral("comment").toString());
			
			if(solution.getResource("thumbnail") != null) {
				identification.setThumbnail(solution.getResource("thumbnail").getURI());
			} else {
				identification.setThumbnail("../images/bola-cinza.png");
			}
			
		}
	}
	
	public void searchUriDBPedia(Identification identification) throws Exception {
			
		try {
		
		StringBuffer sparql = new StringBuffer();
		
		sparql.append(" PREFIX dbo:<http://dbpedia.org/ontology/> ");
		sparql.append(" PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> ");
		sparql.append(" PREFIX dbp: <http://dbpedia.org/property/>  ");		
		
		sparql.append(" SELECT distinct ");
		sparql.append("  * ");		
		sparql.append( " WHERE { ?mammal a dbo:Mammal . ");		
		sparql.append( " ?mammal dbp:binomial ?specie . ");	
		sparql.append( " ?mammal dbo:order ?order. ");
		sparql.append( " ?mammal dbo:family ?family . ");
		sparql.append( " ?mammal dbo:thumbnail ?thumbnail . ");
		sparql.append( " ?family rdfs:label ?familyName . ");
		sparql.append( " ?order rdfs:label ?orderName ");
		
		sparql.append(" FILTER regex(?specie, '" + identification.getScientificName() + "', 'i')   ");
		sparql.append( " FILTER ( lang(?familyName) = 'pt' )" );
		sparql.append( " FILTER ( lang(?orderName) = 'pt' ) . } Limit 1 "  );
		
		
		ResultSet rs = query(sparql.toString(), DataSets.DB_PEDIA);
		
		
		
		rs.hasNext();
			QuerySolution solution = rs.nextSolution();
			identification.setToTaxon( solution.getResource("mammal").getURI());
			identification.setScientificName( solution.getLiteral("specie").getString() );
			identification.setOrder( solution.getLiteral("orderName").getString() );
			identification.setFamily ( solution.getLiteral("familyName").getString() );		
			identification.setThumbnail(solution.getResource("thumbnail").getURI());
		
		
		} catch (Exception e) {
			
			if(error <= 2) {
				error++;
				searchUriDBPedia(identification);
			} else {
				error = 0;
				throw e;
			}
			
		}

	}
	
public List<Identification> searchFamilyDBPedia() {
		
		StringBuffer sparql = new StringBuffer();
		
		sparql.append(" PREFIX dbo:<http://dbpedia.org/ontology/> ");
		sparql.append(" PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> ");
		sparql.append(" PREFIX dbp: <http://dbpedia.org/property/>  ");		
		
		sparql.append(" SELECT distinct ?familyName ");
		sparql.append( " WHERE { ?mammal a dbo:Mammal . ");	
		//sparql.append( " ?mammal dbo:order ?order . ");
		sparql.append( " ?mammal dbo:family ?family . ");
		//sparql.append( " ?order rdfs:label ?orderName . ");
		sparql.append( " ?family rdfs:label ?familyName . ");		
		sparql.append( " FILTER ( lang(?familyName) = 'en' )  "  );
		//sparql.append( " FILTER ( lang(?orderName) = 'en' )  "  );
		//sparql.append(" FILTER regex(?orderName, '" + order + "', 'i')   ");
		sparql.append(" .  } ORDER BY ?family ");
		
		ResultSet rs = query(sparql.toString(), DataSets.DB_PEDIA);
		
		Identification identification;
		List<Identification> families = new ArrayList<Identification>();
		while(rs.hasNext()) { 
			identification = new Identification();
			
			QuerySolution solution = rs.nextSolution();
			identification.setFamily ( solution.getLiteral("familyName").getString() );
			
			families.add(identification);
		}
		
		return families;
		
	}

public List<Identification> searchOrderDBPedia() {
	
	StringBuffer sparql = new StringBuffer();
	
	sparql.append(" PREFIX dbo:<http://dbpedia.org/ontology/> ");
	sparql.append(" PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> ");
	sparql.append(" PREFIX dbp: <http://dbpedia.org/property/>  ");		
	
	sparql.append(" SELECT distinct ?orderName ");		
	sparql.append( " WHERE { ?mammal a dbo:Mammal . ");		
	sparql.append( " ?mammal dbo:order ?order. ");
	sparql.append( " ?order rdfs:label ?orderName ");
	sparql.append( " FILTER ( lang(?orderName) = 'en' )  "  );
	sparql.append(" .  } ORDER BY ?order ");
	
	ResultSet rs = query(sparql.toString(), DataSets.DB_PEDIA);
	
	Identification identification;
	List<Identification> identi = new ArrayList<Identification>();
	while(rs.hasNext()) { 
		identification = new Identification();
		
		QuerySolution solution = rs.nextSolution();
		identification.setOrder( solution.getLiteral("orderName").getString() );
		
		identi.add(identification);
	}
	
	return identi;
	
}

public List<Identification> searchSpeciesDBPedia(String family) {
	
	StringBuffer sparql = new StringBuffer();
	
	sparql.append(" PREFIX dbo:<http://dbpedia.org/ontology/> ");
	sparql.append(" PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> ");
	sparql.append(" PREFIX dbp: <http://dbpedia.org/property/>  ");		
	
	sparql.append(" SELECT distinct ");
	sparql.append("  * ");		
	sparql.append( " WHERE { ?mammal a dbo:Mammal . ");		
	sparql.append( " ?mammal dbp:binomial ?specie . ");	
	sparql.append( " ?family rdfs:label ?familyName . ");	
	
	sparql.append( " ?mammal dbo:family ?family . ");
	sparql.append( " FILTER ( lang(?familyName) = 'en' )  "  );
	
	sparql.append(" FILTER regex(?familyName, '" + family + "', 'i')   ");
	
	sparql.append(" .  } ORDER BY ?specie ");
	
	ResultSet rs = query(sparql.toString(), DataSets.DB_PEDIA);
	
	Identification identification;
	List<Identification> orders = new ArrayList<Identification>();
	while(rs.hasNext()) { 
		identification = new Identification();
		
		QuerySolution solution = rs.nextSolution();
		identification.setId( solution.getResource("?mammal").getURI() );
		identification.setScientificName( solution.getLiteral("specie").getString() );
		
		orders.add(identification);
	}
	
	return orders;
}
	
	public void searchLocalGeonames(Location location) {
		
		try {				
			Model model = ModelFactory.createDefaultModel();
			model.read( "http://sws.geonames.org/3469034/contains.rdf" );
			
			
			StringBuffer sparql = searchGeonames(location.getStateProvidence());			
			ResultSet rs = queryRDF(sparql.toString(), model);			
			rs.hasNext(); 
			QuerySolution solution = rs.nextSolution();			
			String uri = solution.getResource("feature").getURI();	 //uri estado					
			
			model.read( uri + "contains.rdf" );
			sparql = searchGeonames(location.getCounty());							
			rs = queryRDF(sparql.toString(), model);			
			rs.hasNext(); 
			solution = rs.nextSolution();			
			String uriMunicipio = solution.getResource("feature").getURI(); 
			
			//carreguei as localidades
			model.read( uriMunicipio + "contains.rdf" );
			sparql = searchGeonames(location.getMunicipality());			
			rs = queryRDF(sparql.toString(), model);			
			
			String uriLocalidade = null;
			if(rs.hasNext()) {
				solution = rs.nextSolution();			
				uriLocalidade = solution.getResource("feature").getURI();
			}
			
			if(uriLocalidade == null) {
				location.setInDescribedPlace(uriMunicipio);
			} else {
				location.setInDescribedPlace(uriLocalidade);
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			searchLocalGeonames(location);
		}	
			

	}
	
/**
 * Obtem os estados
 * 	
 * @return
 */
public List<Location> getStates() {
	
		List<Location> locations = new ArrayList<Location>();
		
		try {				
			Model model = ModelFactory.createDefaultModel();
			model.read( "http://sws.geonames.org/3469034/contains.rdf" );
			
			
			StringBuffer sparql = searchGeonames();			
			ResultSet rs = queryRDF(sparql.toString(), model);			
			
			QuerySolution solution;			
			Location location;			
			while(rs.hasNext()) {
				solution = rs.nextSolution();
				location = new Location();
				location.setId( solution.getResource("feature").getURI() );
				location.setStateProvidence( solution.getLiteral("name").getString() );
				
				locations.add(location);
			}
			
			
		} catch (Exception e) {
			//getStates();
			e.printStackTrace();
		}				

		return locations;
		
	}

	/**
	 * Obtém o município
	 * @param stateUri
	 * @return
	 */
	public List<Location> getCounty(String stateUri) {
		
		ResultSet rs = navGeoNamesRs(stateUri + "contains.rdf");
		
		List<Location> locations = new ArrayList<Location>();

		QuerySolution solution;
		Location location;
		while(rs.hasNext()) {
			
			solution = rs.nextSolution();
			location = new Location();
			location.setInDescribedPlace( solution.getResource("feature").getURI() );
			location.setCounty( solution.getLiteral("name").getString() );
			
			locations.add(location);
		}
		
		return locations;
		
	}

	private StringBuffer searchGeonames(String search) {
		
		
		StringBuffer sparql = new StringBuffer();
		
		sparql.append(" PREFIX gn:<http://www.geonames.org/ontology#> ");
		//sparql.append(" PREFIX wgs84_pos:<http://www.w3.org/2003/01/geo/wgs84_pos#>  ");		
		
		sparql.append(" SELECT distinct ");
		sparql.append("  ?feature ");	
		sparql.append(" WHERE { ?feature a gn:Feature . ");		
		sparql.append(" ?feature gn:name ?name . ");		
		sparql.append(" filter regex(str(?name),'" + search + "', 'i' ) .  } LIMIT 1 ");
		
		return sparql;

	}
	
	private StringBuffer searchGeonames() {
		
		
		StringBuffer sparql = new StringBuffer();
		
		sparql.append(" PREFIX gn:<http://www.geonames.org/ontology#> ");
		
		sparql.append(" SELECT distinct ");
		sparql.append("  ?feature ?name ");	
		sparql.append(" WHERE { ?feature a gn:Feature . ");		
		sparql.append(" ?feature gn:name ?name  ");		
		sparql.append(" .  } ORDER BY ?name ");
		
		return sparql;

	}

	private void searchGeonames(Location location) {
		
		String endereco = location.getInDescribedPlace() + "about.rdf";
		Model model = ModelFactory.createDefaultModel();		
		model.read( endereco );
		
		StringBuffer sparql = new StringBuffer();
		
		sparql.append(" PREFIX gn:<http://www.geonames.org/ontology#> ");
		sparql.append(" PREFIX wgs84_pos:<http://www.w3.org/2003/01/geo/wgs84_pos#> ");
				
		sparql.append(" SELECT distinct ");
		sparql.append("  * ");	
		sparql.append(" WHERE { ?feature a gn:Feature . ");		
		sparql.append(" ?feature gn:name ?name ; ");
		sparql.append("          gn:parentFeature ?parent ; ");
		sparql.append("  		 gn:parentADM1 ?adm1 ; ");
		sparql.append(" 	     wgs84_pos:lat ?lat ; ");
		sparql.append(" 	     wgs84_pos:long ?long . ");
		
		sparql.append(" } LIMIT 1 ");
		
		
		ResultSet rs = queryRDF(sparql.toString(), model);			
		rs.hasNext(); 
		QuerySolution solution = rs.nextSolution();
		
		String feature = solution.getResource("feature").getURI();
		String parent = solution.getResource("parent").getURI();
		String adm1 = solution.getResource("adm1").getURI();
		
		
		
		if(adm1.equals(parent)) {
			//municipio			
			location.setCounty(solution.getLiteral("name").getString());
			
			//parent é o estado
			endereco = solution.getResource("parent").getURI() + "about.rdf";
			solution = navGeoNames(endereco);
		
			location.setStateProvidence(solution.getLiteral("name").getString());
			
			//obter o bairro, pois a hierarquia do gonames começou pelo município
			searchMunicipality(location);
		} else {	
			
			//bairro
			location.setMunicipality(solution.getLiteral("name").getString());
			
			location.setDecimalLatitude(solution.getLiteral("lat").getDouble());
			location.setDecimalLongitude(solution.getLiteral("long").getDouble());
			
			//o parent é o municipio, pq comoçou pelo bairro a hierarquia		
			endereco = solution.getResource("parent").getURI() + "about.rdf";
			solution = navGeoNames(endereco);
			location.setCounty(solution.getLiteral("name").getString());
			
			//parent é o estado
			endereco = solution.getResource("parent").getURI() + "about.rdf";
			solution = navGeoNames(endereco);
		
			location.setStateProvidence(solution.getLiteral("name").getString());
		}

	}

	private QuerySolution navGeoNames(String endereco) {
		
		Model model = ModelFactory.createDefaultModel();		
		model.read( endereco );
		
		StringBuffer sparql = new StringBuffer();
		
		sparql.append(" PREFIX gn:<http://www.geonames.org/ontology#> ");		
		
		sparql.append(" SELECT distinct ");
		sparql.append("  * ");	
		sparql.append(" WHERE { ?feature a gn:Feature ; ");		
		sparql.append("          gn:parentFeature ?parent ; ");
		sparql.append(" 		 gn:name ?name . ");
		
		sparql.append(" } ");
		
		ResultSet rs = queryRDF(sparql.toString(), model);			
		rs.hasNext(); 
		
		QuerySolution solution = rs.nextSolution();
		
		
		return solution;
	}
	
private ResultSet navGeoNamesRs(String endereco) {
		
		Model model = ModelFactory.createDefaultModel();		
		model.read( endereco );
		
		StringBuffer sparql = new StringBuffer();
		
		sparql.append(" PREFIX gn:<http://www.geonames.org/ontology#> ");		
		
		sparql.append(" SELECT distinct ");
		sparql.append("  * ");	
		sparql.append(" WHERE { ?feature a gn:Feature ; ");		
		sparql.append("          gn:parentFeature ?parent ; ");
		sparql.append(" 		 gn:name ?name . ");
		
		sparql.append(" } ORDER BY ?name ");
		
		ResultSet rs = queryRDF(sparql.toString(), model);
		
		return rs; 
	}
	

	//bairro
public void searchMunicipality(Location location) {

		
		try {
			StringBuffer sparql = new StringBuffer();
	
			sparql.append(" prefix dwc: <http://rs.tdwg.org/dwc/terms/> ");
			sparql.append(" prefix dwciri: 	<http://rs.tdwg.org/dwc/iri/> ");	
			sparql.append(" prefix dcterms: <http://purl.org/dc/terms/> ");
			
			sparql.append(" SELECT distinct ?municipality ?long ?lat ?locationRemarks ?location ");		
			sparql.append( " WHERE { ?location a dcterms:Location; ");	
			sparql.append(" dwc:municipality  ?municipality ;");
			sparql.append(" dwc:decimalLatitude  ?lat ; ");
			sparql.append(" dwciri:inDescribedPlace  ?inDescribedPlace ; ");
			sparql.append(" dwc:decimalLongitude  ?long ; ");
			sparql.append(" dwc:locationRemarks  ?locationRemarks . ");
			
			sparql.append(" filter (?inDescribedPlace = <" + location.getInDescribedPlace() + ">) .  } LIMIT 1 ");
	
			
			ResultSet rs = query(sparql.toString());
	
			rs.hasNext() ; 
			QuerySolution solution = rs.nextSolution();	        
	        location.setMunicipality(solution.getLiteral("municipality").getString());	        
	        location.setDecimalLatitude(solution.getLiteral("lat").getDouble());
	        location.setDecimalLongitude(solution.getLiteral("long").getDouble());
	        location.setLocationRemarks(solution.getLiteral("locationRemarks").getString());	 
	        
	        location.setLocationBN(solution.getResource("location"));
		
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

//obter o bairro
public Resource getLocality(Location location) {

		StringBuffer sparql = new StringBuffer();

		sparql.append(" prefix dwc: <http://rs.tdwg.org/dwc/terms/> ");
		sparql.append(" prefix dwciri: 	<http://rs.tdwg.org/dwc/iri/> ");	
		sparql.append(" prefix dcterms: <http://purl.org/dc/terms/> ");		
		
		sparql.append(" SELECT distinct ?location ");		
		sparql.append( " WHERE { ?location a dcterms:Location; ");	
		sparql.append(" dwc:municipality  ?municipality ;");
		sparql.append(" dwciri:inDescribedPlace  ?inDescribedPlace .");
		
		sparql.append(" filter regex(str(?municipality),'" + location.getMunicipality() + "', 'i' )  ");
		sparql.append(" filter (?inDescribedPlace = <" + location.getInDescribedPlace() + ">) .  } LIMIT 1 ");
		
		ResultSet rs = query(sparql.toString());

		if(rs.hasNext()) {
			QuerySolution solution = rs.nextSolution();
	        return solution.getResource("location");
		}
		
		return null;

}
	
	
}
