package br.ufrj.macae.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

public class Event implements Serializable, Cloneable {
		
	/**
	 * 
	 */
	private static final long serialVersionUID = -2656432454273314302L;
	
	private String uri;

	private List<SamplingProtocol> samplingProtocol;	
	
	private String eventRemarks; 
	private Date eventDate;	
	
	private List<Recorder> recordedByList;
	
	private String recordedBy;
	
	private String fieldNotes;
	
	private String protocol;
	
	
	@Inject 
	private Location location;

	@Inject
	private Identification identification;// é ligado com a ocorrẽncia no rdf
	
	public Event clone() throws CloneNotSupportedException{
		return (Event) super.clone();
	}
		
	
	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Identification getIdentification() {
		return identification;
	}

	public void setIdentification(Identification identification) {
		this.identification = identification;
	}

	
	public List<SamplingProtocol> getSamplingProtocol() {
		return samplingProtocol;
	}

	public void setSamplingProtocol(List<SamplingProtocol> samplingProtocol) {
		this.samplingProtocol = samplingProtocol;
	}

	public String getEventRemarks() {
		return eventRemarks;
	}

	public void setEventRemarks(String eventRemarks) {
		this.eventRemarks = eventRemarks;
	}

	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	public List<Recorder> getRecordedByList() {
		return recordedByList;
	}

	public void setRecordedByList(List<Recorder> recordedByList) {
		this.recordedByList = recordedByList;
	}

	public String getFieldNotes() {
		return fieldNotes;
	}

	public void setFieldNotes(String fieldNotes) {
		this.fieldNotes = fieldNotes;
	}

	public String getRecordedBy() {
		return recordedBy;
	}

	public void setRecordedBy(String recordedBy) {
		this.recordedBy = recordedBy;
	}


	public String getProtocol() {
		return protocol;
	}


	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}
	

}
