package br.ufrj.macae.entity;

import java.io.Serializable;

import org.apache.jena.rdf.model.Resource;

public class Identification implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6164702515674369285L;
	
	private String id;
	private String scientificName;
	private String family;
	private String order;
	
	//referência para uma classificação taxonomica em outro repositório
	private String toTaxon;
	
	private Resource identicationBN;
	
	private String seeAlso;
	private String comment;
	private String thumbnail;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getScientificName() {
		return scientificName;
	}
	public void setScientificName(String scientificName) {
		this.scientificName = scientificName;
	}
	public String getFamily() {
		return family;
	}
	public void setFamily(String family) {
		this.family = family;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public String getToTaxon() {
		return toTaxon;
	}
	public void setToTaxon(String toTaxon) {
		this.toTaxon = toTaxon;
	}
	public Resource getIdenticationBN() {
		return identicationBN;
	}
	public void setIdenticationBN(Resource identicationBN) {
		this.identicationBN = identicationBN;
	}
	public String getSeeAlso() {
		return seeAlso;
	}
	public void setSeeAlso(String seeAlso) {
		this.seeAlso = seeAlso;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	
	
}
