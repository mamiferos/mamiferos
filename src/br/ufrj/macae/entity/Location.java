package br.ufrj.macae.entity;

import java.io.Serializable;
import java.util.Date;

import javax.inject.Inject;

import org.apache.jena.rdf.model.Resource;

public class Location implements Serializable {
		
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8652094332667274139L;


	private String id;
	
	private String stateProvidence;
	
	private String county;// município
	private String municipality;//bairro
	
	//geonames
	private String inDescribedPlace = "";//uri município 
	
	private Double decimalLongitude;
	private Double decimalLatitude;	
	private String locationRemarks;
	
	private Resource locationBN;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	
	
	public String getStateProvidence() {
		return stateProvidence;
	}
	public void setStateProvidence(String stateProvidence) {
		this.stateProvidence = stateProvidence;
	}
	public String getMunicipality() {
			
		return municipality;
	}
	public void setMunicipality(String municipality) {
		this.municipality = municipality;
	}
	
	public String getInDescribedPlace() {
		return inDescribedPlace;
	}
	public void setInDescribedPlace(String inDescribedPlace) {
		this.inDescribedPlace = inDescribedPlace;
	}
	
	
	public String getLocationRemarks() {
		return locationRemarks;
	}
	public void setLocationRemarks(String locationRemarks) {
		this.locationRemarks = locationRemarks;
	}
	public Resource getLocationBN() {
		return locationBN;
	}
	public void setLocationBN(Resource locationBN) {
		this.locationBN = locationBN;
	}
	public String getCounty() {
		return county;
	}
	public void setCounty(String county) {
		this.county = county;
	}
	public Double getDecimalLongitude() {
		return decimalLongitude;
	}
	public void setDecimalLongitude(Double decimalLongitude) {
		this.decimalLongitude = decimalLongitude;
	}
	public Double getDecimalLatitude() {
		return decimalLatitude;
	}
	public void setDecimalLatitude(Double decimalLatitude) {
		this.decimalLatitude = decimalLatitude;
	}
	
	
	
	
}
