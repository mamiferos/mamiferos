package br.ufrj.macae.entity;

import java.io.Serializable;

import org.apache.jena.rdf.model.Resource;

public class MeasurementOrFact implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2814366732959299159L;
	
	private String measurementType;
	private String measurementValue;
	private String measurementUnit;
	
	private String measurementRemarks;
	
	private Resource occurrence;
	
	public MeasurementOrFact clone() throws CloneNotSupportedException{
		return (MeasurementOrFact) super.clone();
	}
		
	public String getMeasurementType() {
		return measurementType;
	}
	public void setMeasurementType(String measurementType) {
		this.measurementType = measurementType;
	}
	
	public String getMeasurementValue() {
		return measurementValue;
	}
	public void setMeasurementValue(String measurementValue) {
		this.measurementValue = measurementValue;
	}
	public String getMeasurementUnit() {
		return measurementUnit;
	}
	public void setMeasurementUnit(String measurementUnit) {
	
		this.measurementUnit = measurementUnit;
	}
	
	public String getMeasurementRemarks() {
		return measurementRemarks;
	}
	public void setMeasurementRemarks(String measurementRemarks) {
		this.measurementRemarks = measurementRemarks;
	}
	public Resource getOccurrence() {
		return occurrence;
	}
	public void setOccurrence(Resource occurrence) {
		this.occurrence = occurrence;
	}	
	
	
}
