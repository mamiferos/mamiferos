package br.ufrj.macae.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.apache.jena.rdf.model.Resource;


public class Occurrence implements Serializable, Cloneable {
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1525721362280980323L;

	private String uri;
	
	private String catalogNumber;//tombo
	private String recordNumber;// número do campo
	private String occurrenceRemarks;
	private String sex;
	
	private String locality; //estação de coleta
		
	private String preparations; //armazenamento na coleção
	private String disposition;//status na coleção	
			
	private List<MeasurementOrFact> measurements = new ArrayList<MeasurementOrFact>();
	
	@Inject
	private Event Event;
	
	private Resource identification;
	private Resource occurrence;
	
	public Occurrence clone() throws CloneNotSupportedException{
		return (Occurrence) super.clone();
	}
	
	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getRecordNumber() {
		return recordNumber;
	}

	public void setRecordNumber(String recordNumber) {
		this.recordNumber = recordNumber;
	}

	public String getOccurrenceRemarks() {
		return occurrenceRemarks;
	}

	public void setOccurrenceRemarks(String occurrenceRemarks) {
		this.occurrenceRemarks = occurrenceRemarks;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getDisposition() {
		return disposition;
	}

	public void setDisposition(String disposition) {
		this.disposition = disposition;
	}
	

	public List<MeasurementOrFact> getMeasurements() {
		return measurements;
	}

	public void setMeasurements(List<MeasurementOrFact> measurements) {
		this.measurements = measurements;
	}

	public Event getEvent() {
		return Event;
	}

	public void setEvent(Event event) {
		Event = event;
	}

	public String getCatalogNumber() {
		return catalogNumber;
	}

	public void setCatalogNumber(String catalogNumber) {
		this.catalogNumber = catalogNumber;
	}

	public String getPreparations() {
		return preparations;
	}

	public void setPreparations(String preparations) {
		this.preparations = preparations;
	}

	public Resource getIdentification() {
		return identification;
	}

	public void setIdentification(Resource identification) {
		this.identification = identification;
	}

	public Resource getOccurrence() {
		return occurrence;
	}

	public void setOccurrence(Resource occurrence) {
		this.occurrence = occurrence;
	}

	public String getLocality() {
		return locality;
	}

	public void setLocality(String locality) {
		this.locality = locality;
	}
	
	
}
