package br.ufrj.macae.entity;

import java.io.Serializable;

public class Recorder implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3134303453532769800L;

	private String id;
	
	private String name;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
}
