package br.ufrj.macae.entity;

import java.io.Serializable;

public class Researcher extends Recorder implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3877675858969163694L;
	
	private String uri;
	private String surname;//nomes em obras científicas
	private String email;	
	
	//vCard
	private String key;//senha

	
	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	
	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
	
	

}
