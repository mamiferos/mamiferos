package br.ufrj.macae.entity;

public class SamplingProtocol implements java.io.Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1870719343935563062L;
	
	private long id;
	private String samplingProtocol;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getSamplingProtocol() {
		return samplingProtocol;
	}
	public void setSamplingProtocol(String samplingProtocol) {
		this.samplingProtocol = samplingProtocol;
	}
	
}
