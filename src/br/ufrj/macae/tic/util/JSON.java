package br.ufrj.macae.tic.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map.Entry;
import java.util.Set;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class JSON {
	
	public static void ler(String urlApi) {
		HttpURLConnection con = null;
		try {
			URL url = new URL(urlApi);
			con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			con.connect();

			switch (con.getResponseCode()) {
			case 200:
				//System.out.println("JSON recebido!");
				String json = getJson(url);

				JsonParser parser = new JsonParser();
				JsonObject obj = (JsonObject) parser.parse(json);
				
				

				Set<Entry<String, JsonElement>> el = obj.entrySet();

				for (Entry<String, JsonElement> els : el) {
					if (els.getKey().equals("status")) {
						System.out.println(els.getKey() + ":" + els.getValue().getAsBoolean());
					} else if (els.getKey().equals("valores")) {
						JsonElement e = els.getValue();
						JsonObject jobj = e.getAsJsonObject();
						Set<Entry<String, JsonElement>> props = jobj.entrySet();
						for (Entry<String, JsonElement> prop : props) {
							System.out.println(prop.getKey() + ":" + String.valueOf(prop.getValue()));
						}
					}
				}
				break;
			case 500:
				System.out.println("Status 500");
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (con != null)
				con.disconnect();
		}

	}

	public static String getJson(URL url) {
		if (url == null)
			throw new RuntimeException("URL é null");

		String html = null;
		StringBuilder sB = new StringBuilder();
		try (BufferedReader bR = new BufferedReader(new InputStreamReader(url.openStream()))) {
			while ((html = bR.readLine()) != null)
				sB.append(html);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return sB.toString();
	}
}