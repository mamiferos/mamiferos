package br.ufrj.macae.tic.util;

public enum SamplingProtocolEnum {
	
	PITFALL("PitFall"), SHERMAN("Sherman"), TOMARROCK("Tomahawk"), REDE_NEBLINA("Rede de neblina"), 
	ARMADILHA("Armadilha"), PREDAÇÃO("Predação"), APREENSAO("Apreensão de caça"), 
	COLETA_MANUAL("Coleta manual"), CAPTURA_ACIDENTAL("Captura acidental"); 
    
    private final String samplingProtocol;
    
    SamplingProtocolEnum(String samplingProtocolValue){
    	samplingProtocol = samplingProtocolValue;
    }
    
    public String getSamplingProtocol(){
        return samplingProtocol;
    }

}
