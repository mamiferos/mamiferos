package br.ufrj.macae.controller;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.RequestScoped;
import javax.inject.Named;

import org.junit.Test;

import br.ufrj.macae.dao.OccurrenceDAOTest;
import br.ufrj.macae.entity.Event;
import br.ufrj.macae.entity.Identification;
import br.ufrj.macae.entity.Location;
import br.ufrj.macae.entity.MeasurementOrFact;
import br.ufrj.macae.entity.Occurrence;
import br.ufrj.macae.entity.SamplingProtocol;

@Named
@RequestScoped
public class OccurrenceMBTest {
	
	
	private Occurrence occurrence = new Occurrence();
	
	private List<Occurrence> occurrenceList;
	

	@Test
	public void testInsert() {
		
		try {			
			
		Event event = testEvent();
		
		occurrence.setEvent(event);		
		
		occurrence.setCatalogNumber("PRG1379");
		occurrence.setRecordNumber("NPM001");
		occurrence.setOccurrenceRemarks("atropelado, pele em bom estado, coletado por Ederson (guarda municipal Rio das Ostras");
		occurrence.setSex("F");
		occurrence.setDisposition("OK");
		occurrence.setPreparations("Meio seco");
		
		
		MeasurementOrFact measurementOrFact = new MeasurementOrFact();
	
		/*
		occurrence.setMeasurement(measurementOrFact);
		occurrence.getMeasurement().setMeasurementType("orelha");
		occurrence.getMeasurement().setMeasurementValue("65.00");
		*/
		
		OccurrenceDAOTest occurrenceDAO = new OccurrenceDAOTest();
		
		occurrenceDAO.insert(occurrence);
		
		
		} catch (Exception e) {
			
			e.printStackTrace();
			fail(e.getMessage());			
		}
		
		
		
	}
	
	public Event testEvent() {
		
		Event event = new Event();	
		
		try {
			
		
		SamplingProtocol samplingProtocol = new SamplingProtocol();
		samplingProtocol.setSamplingProtocol("PitFal");
		
		List<SamplingProtocol> sp = new ArrayList<SamplingProtocol>();
		sp.add(samplingProtocol);
		
		event.setSamplingProtocol(sp);
		
		event.setEventRemarks("Atropelado");
		
		event.setEventDate(new Date());
				
		Identification taxon = new Identification();
		event.setIdentification(taxon);
		taxon.setFamily("Familia");
		taxon.setOrder("order");
		taxon.setScientificName("scientificName");
		taxon.setToTaxon("http://dbpedia.org/resource/Crab-eating_fox");
		
		Location location = new Location();
		
		location.setLocationRemarks("ARIE de Itapebussus, Rio das Ostras, RJ. Restinga");
		location.setDecimalLatitude(20.00);
		location.setDecimalLongitude(30.00);
		location.setCounty("Rio das Ostras");
		location.setStateProvidence("RJ");
		location.setMunicipality("Arie de Itabebussus");
		location.setInDescribedPlace("http://sws.geonames.org/3451205/");
		
		event.setLocation(location);
				
		
		
		} catch (Exception e) {
			
			e.printStackTrace();
			fail(e.getMessage());			
		}
		
		
		return event;
		
	}

}
