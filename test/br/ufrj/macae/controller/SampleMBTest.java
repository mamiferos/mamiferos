package br.ufrj.macae.controller;

import static org.junit.Assert.fail;

import java.util.List;

import javax.faces.bean.RequestScoped;
import javax.inject.Named;

import org.junit.Test;

import br.ufrj.macae.entity.Event;
import br.ufrj.macae.entity.Identification;
import br.ufrj.macae.entity.Location;
import br.ufrj.macae.entity.Occurrence;
import br.ufrj.macae.entity.Researcher;
import br.ufrj.macae.entity.SamplingProtocol;

@Named
@RequestScoped
public class SampleMBTest {
	
	//private static final OccurrenceDAOTest occurrenceDAO = new OccurrenceDAOTest();
	
	private Occurrence occurrence = new Occurrence();
	
	private List<Occurrence> occurrenceList;
	

	@Test
	public void testInsert() {
		
		try {
			
			
		Event event = new Event();
		Researcher person = new Researcher();
				
		
		SamplingProtocol samplingProtocol = new SamplingProtocol();
		samplingProtocol.setSamplingProtocol("PitFal");
		//event.setSamplingProtocol(samplingProtocol);
		
		//event.setEventRemarks("Atropelado");
		//event.setEventDate(new Date("10-10-10"));
		
		person.setUri("person1");
		
		Identification taxon = new Identification();
		//event.setTaxon(taxon);
		taxon.setFamily("family");
		taxon.setOrder("order");
		taxon.setScientificName("Cerdocyon thous");
		//taxon.setUri("http://dbpedia.org/resource/Crab-eating_fox");
		
		Location l = new Location();
		l.setStateProvidence("Rio de Janeiro");
		l.setCounty("Macaé");
		event.setLocation(l);
		
		/*
		event.setLocationRemarks("ARIE de Itapebussus, Rio das Ostras, RJ. Restinga");
		event.setDecimalLatitude(10.00);
		event.setDecimalLongitude(20.00);
		event.setMunicipality("Rio das Ostras");
		event.setState("RJ");
		event.setLocality("Arie de Itabebussus");
		event.setInDescribedPlace("http://sws.geonames.org/3451205/");
		*/
		
		event.setIdentification(taxon);
		
		SampleMB s = new SampleMB();
		s.setEventSave(event);
		s.save();
		
		
		} catch (Exception e) {
			
			e.printStackTrace();
			fail(e.getMessage());			
		}
		
		
		
	}

}
