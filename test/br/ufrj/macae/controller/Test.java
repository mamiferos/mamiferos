package br.ufrj.macae.controller;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;

public class Test {
	

	
	public static void main(String[] args) throws Exception {
	
		try {
			
			Model model = ModelFactory.createDefaultModel();
			
			model.read("http://sws.geonames.org/3451189/about.rdf");
			
			Resource vcard = model.getResource("http://sws.geonames.org/3451189/");
			
			
			Property name =  model.createProperty("http://www.geonames.org/ontology#name");
			Property countryP =  model.createProperty("http://www.geonames.org/ontology#parentCountry");
			
			Resource country = model.getResource("http://sws.geonames.org/3469034/");
			String t = country.getProperty(name).getString();
			
			model.write(System.out, "turtle");
			
		} catch (Exception e) {			
			e.printStackTrace();
		}

		
	}
}
