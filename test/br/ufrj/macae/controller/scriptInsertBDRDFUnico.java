package br.ufrj.macae.controller;

import static org.junit.Assert.fail;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.RequestScoped;
import javax.inject.Named;

import org.apache.poi.ss.formula.functions.Replace;
import org.junit.Test;

import br.ufrj.macae.dao.OccurrenceDAOTest;
import br.ufrj.macae.dao.OccurrenceDAOTestUnico;
import br.ufrj.macae.entity.Event;
import br.ufrj.macae.entity.Identification;
import br.ufrj.macae.entity.Location;
import br.ufrj.macae.entity.MeasurementOrFact;
import br.ufrj.macae.entity.Occurrence;
import br.ufrj.macae.entity.SamplingProtocol;
import br.ufrj.macae.tic.util.Util;

@Named
@RequestScoped
public class scriptInsertBDRDFUnico {
	
	
	private Occurrence occurrence = new Occurrence();
	
	private List<Occurrence> occurrenceList;
	
	private List<MeasurementOrFact> measurementList;
		
	private MeasurementOrFact measurementOrFact;
	

	@Test
	public void testInsert() {
		
		try {			
			
			Connection conn = getConnection();
			
			//String sql = "SELECT * FROM dado";
			String sql = "SELECT * FROM dado where id = 1";
		    PreparedStatement stmt = conn.prepareStatement(sql);
		    ResultSet rs= stmt.executeQuery();

		    while(rs.next()) {		    	

				Event event = new Event();	
				
		        //System.out.println(rs.getString("nome_do_campo"));
		        
		        SamplingProtocol samplingProtocol = new SamplingProtocol();
				samplingProtocol.setSamplingProtocol(rs.getString("tecnicaColeta"));
				
				List<SamplingProtocol> sp = new ArrayList<SamplingProtocol>();
				sp.add(samplingProtocol);
				
				event.setSamplingProtocol(sp);
				
				event.setEventRemarks(rs.getString("obs"));
				
				//31.X.2008
				String data = rs.getString("data");
				data = acertarData(data);				
										
				Identification taxon = new Identification();
				event.setIdentification(taxon);
				taxon.setFamily(rs.getString("familia"));
				taxon.setOrder( rs.getString("ordem") );
				taxon.setScientificName(rs.getString("especie"));
				
				//taxon.setToTaxon("http://dbpedia.org/resource/Crab-eating_fox");
				
				Location location = new Location();
				
				organizarLocalidade(rs.getString("localidade"), location);
				
				Double c = Util.tranformarCoordenadasDecimal(rs.getString("latitude"));				
				location.setDecimalLatitude(c);
				
				c = Util.tranformarCoordenadasDecimal(rs.getString("longitude"));
				location.setDecimalLongitude(c);
				
				//location.setInDescribedPlace("http://sws.geonames.org/3451205/");
				
				event.setLocation(location);
				
				//ocorrencia
				
				occurrence.setEvent(event);		
				
				occurrence.setCatalogNumber(rs.getString("numeroCampo"));
				occurrence.setRecordNumber(rs.getString("numeroTombo"));
				//occurrence.setOccurrenceRemarks(rs.getString("numeroCampo"));
				occurrence.setSex(rs.getString("sexo"));
				occurrence.setDisposition(rs.getString("statusColecao"));
				occurrence.setPreparations(rs.getString("armazenamentoColecao"));
				
				//occurrence.setRecordedBy(rs.getString("coletor"));
				

				measurementList = new ArrayList<MeasurementOrFact>();
				
				setMeasurement("cTotal", rs.getString("cTotal").replace(",", "."), rs.getString("medidas"));
				setMeasurement("cCorpo", rs.getString("cCorpo").replace(",", "."), rs.getString("medidas") );
				setMeasurement("cCauda", rs.getString("cCauda").replace(",", "."),rs.getString("medidas") );
				setMeasurement("calcar", rs.getString("calcar").replace(",", ".") , rs.getString("medidas"));
				setMeasurement("pé", rs.getString("pe"), rs.getString("medidas") );
				setMeasurement("orelha", rs.getString("orelha").replace(",", "."), rs.getString("medidas"));
				setMeasurement("trago", rs.getString("trago").replace(",", "."), rs.getString("medidas"));
				setMeasurement("anteBraco", rs.getString("anteBraco").replace(",", "."), rs.getString("medidas"));
				setMeasurement("peso", rs.getString("peso").replace(",", "."), rs.getString("medidas"));
				
				//occurrence.setMeasurementList(measurementList);
				
				OccurrenceDAOTestUnico occurrenceDAO = new OccurrenceDAOTestUnico();
				
				occurrenceDAO.insert(occurrence, data);
				
		    }	
		
		
		} catch (Exception e) {
			
			e.printStackTrace();
			fail(e.getMessage());			
		}
		
		
		
	}

	private String acertarData(String data) {
		
		if("".equals(data) || "-".equals(data)) {
			return "";
		}
		
		int index = data.indexOf(".");
		
		if(index < 1) {
			return data;
		}
		
		String aux = data.substring(index+1, data.length());
		int taux = aux.indexOf(".");
		
		String chave; 
		if(taux < 1) {
			chave = data.substring(0, index);
		} else {
			chave = data.substring(index+1, index+taux+1);	
		}
		
		String mes = "";
		switch (chave) {
		
			case "I":
				mes = "01";
				break;
				
			case "II":
				mes = "02";
				break;	
				
			case "III":
				mes = "03";
				break;
				
			case "IV":
				mes = "04";
				break;	
				
			case "V":
				mes = "05";
				break;
			
			case "VI":
				mes = "06";
				break;	
			
			case "VII":
				mes = "07";
				break;	
				
			case "VIII":
				mes = "08";
				break;	
			
			case "IX":
				mes = "09";
				break;	
				
			case "X":
				mes = "10";
				break;	
				
			case "XI":
				mes = "11";
				break;	
				
			case "XII":
				mes = "12";
				break;	
		}
		
		data = data.replace(chave, mes);
		data = data.replace(".", "/");
		
		return data;
	}
	
	
	private void setMeasurement(String tipo, String valor, String medidas) throws SQLException {
		
		//if(!"-".equals(valor)) {
			measurementOrFact = new MeasurementOrFact();
			measurementOrFact.setMeasurementType(tipo);			
			measurementOrFact.setMeasurementValue(valor);	
			measurementOrFact.setMeasurementRemarks(medidas);	
			measurementList.add(measurementOrFact);
		//}
		
	}
	
	private void organizarLocalidade(String local, Location location) {	
		
	try {
			
		
		//Rio das Ostras, RJ
		local = local.replace("loc:", "");
		local = local.replace(";", ",");
		local = local.replace("-", ",");
		local = local.replace("Faz. São Lázaro", "Faz São Lázaro");
		
		//qt v
		
		int qtdVirgulas = qtdVirgulas(local);
		
		int tamanho;
		int aux;
		String valor;		
		if(qtdVirgulas > 2) {
			
			tamanho = local.length();
			aux = local.indexOf(".");
			valor = local.substring(aux +1, tamanho);
			
			aux = local.indexOf(",");
			String valorAux = local.substring(0,aux+1);
			

			local =  local.replace(valor, "");
			local = local.replace(valorAux, "");
			valorAux = valorAux.replace(",", "");
		
			
			valorAux = valorAux + " " + valor; 
			
			location.setLocationRemarks(valorAux);
			
			
		} else {
			
			tamanho = local.length();
			aux = local.indexOf(".");
			valor = local.substring(aux +1, tamanho);
			location.setLocationRemarks(valor.trim());
		}
		
		
		//local = local.replace("Biotério , NUPEM/UFRJ", "Biotério NUPEM/UFRJ");
		
		if("".equals(local.trim())) {
			return;
		}
		
		local = local.replace(valor, "");
		local = local.replace(".", "");		
		tamanho = local.length();
		aux = local.indexOf(",");
		
		valor = local.substring(0, aux);
		location.setMunicipality(valor.trim());
		
		local = local.substring(aux+1, tamanho);
		
		tamanho = local.length();
		aux = local.indexOf(",");		
		valor = local.substring(0, aux);
		location.setCounty(valor.trim());
		
		valor = local.substring(aux+1, tamanho);
		location.setStateProvidence(valor.trim());
		
	} catch (Exception e) {
		e.printStackTrace();
	}	
		
	}

	private int qtdVirgulas(String local) {
		int cont = 0;
		
		String v;
		int aux = local.indexOf(",");
		while (aux > 0) {			
			cont++;	
			v = local.substring(0, aux+1);
			local = local.replace(v, "");
			aux = local.indexOf(",");
		}		
		
		return cont;
	}
	
	public Connection getConnection() throws SQLException {
	     try {
	    	 //jdbc:mysql://localhost/colecao
	         Class.forName("com.mysql.jdbc.Driver");
	         return DriverManager.getConnection("jdbc:mysql://localhost:3306/colecao",
	"root","tic123");
	         
	     } catch (ClassNotFoundException e) {
	         throw new SQLException(e.getMessage());
	     }
	}

}
