package br.ufrj.macae.dao;

import java.util.Date;
import java.util.List;

import org.apache.jena.atlas.io.IndentedWriter;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdf.model.InfModel;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.reasoner.ValidityReport;
import org.apache.jena.sparql.vocabulary.FOAF;
import org.apache.jena.util.FileManager;
import org.apache.jena.vocabulary.DCTerms;
import org.apache.jena.vocabulary.RDF;

import br.ufrj.macae.constante.DarwinCoreTerms;
import br.ufrj.macae.constante.NameSpace;
import br.ufrj.macae.entity.Event;
import br.ufrj.macae.entity.Identification;
import br.ufrj.macae.entity.Location;
import br.ufrj.macae.entity.MeasurementOrFact;
import br.ufrj.macae.entity.Occurrence;
import br.ufrj.macae.tic.util.Util;

public class OccurrenceDAOTest extends GenericDAO {

	public void insert(Occurrence occurrence) {
		
		try {
			
			Event event = occurrence.getEvent();
						
			Resource eventBN = model.createResource();
			
			model.add(eventBN, 
					RDF.type, 
					model.createResource(DarwinCoreTerms.EVENT));
					
			add(eventBN, DarwinCoreTerms.SAMPLING_PROTOCOL, event.getSamplingProtocol().get(0).getSamplingProtocol());
			add(eventBN, DarwinCoreTerms.EVENT_REMARKS, event.getEventRemarks());
			add(eventBN, DarwinCoreTerms.EVENT_DATE, event.getEventDate().toString());		
			
			Resource identificationBN = model.createResource();
			model.add(identificationBN, 
					RDF.type, 
					model.createResource(DarwinCoreTerms.IDENTIFICATION));
			
			add(identificationBN, DarwinCoreTerms.SCIENTIFIC_NAME, "Lobo");
						
			//Location
			
			Resource locationBN = model.createResource();
						
			model.add(locationBN, 
					RDF.type, 
					DCTerms.Location);
			add(locationBN, DarwinCoreTerms.MUNICIPALITY, "Macaé");
			
			model.add(eventBN, DCTerms.relation, locationBN);		
		
		String uri = NameSpace.URI_BASE + occurrence.getCatalogNumber();
		
		//Occurence
			
		
		Resource occurrenceRS = model.createResource(uri);
		
		model.add(occurrenceRS, 
				RDF.type, 
				model.createResource(DarwinCoreTerms.OCCURRENCE));
		
		add(occurrenceRS, DarwinCoreTerms.RECORD_NUMBER, occurrence.getRecordNumber());		
		add(occurrenceRS, DarwinCoreTerms.OCCURRENCE_REMARKS, occurrence.getOccurrenceRemarks());
		add(occurrenceRS, DarwinCoreTerms.SEX, occurrence.getSex());
		add(occurrenceRS, DarwinCoreTerms.PREPARATIONS, occurrence.getPreparations());
		add(occurrenceRS, DarwinCoreTerms.DISPOSITION, occurrence.getDisposition());
		
		//Measurement
		
		//insertMeasurement(occurrence.getMeasurementList(), occurrenceRS);
		
		
		//Relações 
		model.add(occurrenceRS, DCTerms.relation, eventBN);
		model.add(occurrenceRS, DCTerms.relation, identificationBN);
		
				
		
		} catch (Exception e) {
			e.printStackTrace();
			
		}
				
	}
	
public void insert(Occurrence occurrence, String date) {
		
		try {
			
			Event event = occurrence.getEvent();
					
			
			//event
			Resource eventRS = model.createResource(Util.gerarUri());
			
			model.add(eventRS, 
					RDF.type, 
					model.createResource(DarwinCoreTerms.EVENT));
					
			add(eventRS, DarwinCoreTerms.SAMPLING_PROTOCOL, event.getSamplingProtocol().get(0).getSamplingProtocol());
			
			String remarks = event.getEventRemarks();
			try {
			    add(eventRS, DarwinCoreTerms.EVENT_DATE,Util.formatarDataIso8601(new Date(date)), XSDDatatype.XSDdate);		
			} catch (Exception e) {
				
				if(!"".equals(date)) {
					add(eventRS, DarwinCoreTerms.EVENT_REMARKS, "Data do evento: " + date);
					remarks += " " + "Data do evento: " + date;  
				}
				
			}	
						
			add(eventRS, DarwinCoreTerms.EVENT_REMARKS, remarks);
			
			// obs sobre o local da coleta, foi colocado aqui, pois esse dados é associado a coleta. 
			// O local pode esta relacionado com outras coletas.
			add(eventRS, DarwinCoreTerms.FIELD_NOTES, event.getFieldNotes());
			
			Resource identificationBN = model.createResource();
			model.add(identificationBN, 
					RDF.type, 
					model.createResource(DarwinCoreTerms.IDENTIFICATION));
			
			Identification identification = occurrence.getEvent().getIdentification();
			add(identificationBN, DarwinCoreTerms.SCIENTIFIC_NAME, identification.getScientificName());
			add(identificationBN, DarwinCoreTerms.FAMILY, identification.getFamily());
			add(identificationBN, DarwinCoreTerms.ORDER, identification.getOrder());
			//Location
						
			Resource locationBN = model.createResource();
						
			model.add(locationBN, 
					RDF.type, 
					DCTerms.Location);
			
			Location location = occurrence.getEvent().getLocation();

			add(locationBN, DarwinCoreTerms.STATE_PROVIDENCE, location.getStateProvidence());
			add(locationBN, DarwinCoreTerms.COUNTY, location.getCounty());//município
			add(locationBN, DarwinCoreTerms.MUNICIPALITY, location.getMunicipality()); //bairro
			
			//add(locationBN, DarwinCoreTerms.LOCALITY, location.getLocality()); //estação de coleta
			
			add(locationBN, DarwinCoreTerms.DECIMAL_LATITUDE, location.getDecimalLatitude().toString());
			add(locationBN, DarwinCoreTerms.DECIMAL_LONGITUDE, location.getDecimalLongitude().toString());
			
			add(eventRS, DarwinCoreTerms.LOCATE_AT, locationBN);
			
		
		String uri = NameSpace.URI_BASE + occurrence.getRecordNumber();
		
		//Occurence
			
		
		Resource occurrenceRS = model.createResource(uri);
		
		model.add(occurrenceRS, 
				RDF.type, 
				model.createResource(DarwinCoreTerms.OCCURRENCE));
		
		add(occurrenceRS, DarwinCoreTerms.RECORD_NUMBER, occurrence.getRecordNumber());		
		//add(occurrenceRS, DarwinCoreTerms.OCCURRENCE_REMARKS, occurrence.getOccurrenceRemarks());
		add(occurrenceRS, DarwinCoreTerms.SEX, occurrence.getSex());
		add(occurrenceRS, DarwinCoreTerms.PREPARATIONS, occurrence.getPreparations());
		add(occurrenceRS, DarwinCoreTerms.DISPOSITION, occurrence.getDisposition());
		
		//Measurement
		
		//insertMeasurement(occurrence.getMeasurementList(), occurrenceRS);
		
		
		//Relações 
		add(occurrenceRS, DarwinCoreTerms.AT_EVENT, eventRS);
		model.add(occurrenceRS, DCTerms.relation, identificationBN);
		
		System.out.println("Recurso com a URI: " + uri + " foi adicionado com sucesso! \n");
				
		//select(model);
		
		
		
		} catch (Exception e) {
			e.printStackTrace();
			
		}
				
	}

private void insertMeasurement(List<MeasurementOrFact> list, Resource occurrenceRS) {
	
	for (MeasurementOrFact measurementOrFact : list) {
		
		if("-".equalsIgnoreCase(measurementOrFact.getMeasurementValue()) 
				|| "".equalsIgnoreCase(measurementOrFact.getMeasurementValue()) 
				) {
			
			continue;
		}
			
		
		Resource measurementBN = model.createResource();
		model.add(measurementBN, 
				RDF.type, 
				model.createResource(DarwinCoreTerms.MEASUREMENT_OR_FACT));
		if("pé".equalsIgnoreCase(measurementOrFact.getMeasurementType() ) ) {			
			add(measurementBN, DarwinCoreTerms.MEASUREMENT_VALUE, measurementOrFact.getMeasurementValue());
		} else {			
			add(measurementBN, DarwinCoreTerms.MEASUREMENT_VALUE, measurementOrFact.getMeasurementValue().replaceAll(">", ""), XSDDatatype.XSDfloat);
		}
		add(measurementBN, DarwinCoreTerms.MEASUREMENT_TYPE, measurementOrFact.getMeasurementType());
		add(measurementBN, DarwinCoreTerms.MEASUREMENT_REMARKS, measurementOrFact.getMeasurementRemarks());
		
		model.add(occurrenceRS, DCTerms.relation, measurementBN);
	}
	
}
	
	public static void select(Model model)
    {
        // Create the data.
        // This wil be the background (unnamed) graph in the dataset.
       // Model model = createModel() ;
        
        // First part or the query string 
        String prolog = "PREFIX foaf: <"+FOAF.getURI()+">" ;
        
        // Query string.
       // String queryString = 
        //    "SELECT * WHERE {?s ?p ?o }" ; 
       
        
        String queryString = 
                "SELECT * WHERE {?s a <http://xmlns.com/foaf/0.1/Person> . "
                + " ?s <http://purl.org/dc/elements/1.1/identifier> ?id  "
                + " filter regex(?id,'11111111111', 'i')}" ; 
        
        
        
        Query query = QueryFactory.create(queryString) ;
        // Print with line numbers
        query.serialize(new IndentedWriter(System.out,true)) ;
        System.out.println() ;
        
        // Create a single execution of this query, apply to a model
        // which is wrapped up as a Dataset
        
        try(QueryExecution qexec = QueryExecutionFactory.create(query, model)){
            // Or QueryExecutionFactory.create(queryString, model) ;


            // Assumption: it's a SELECT query.
            ResultSet rs = qexec.execSelect() ;
            
            ResultSetFormatter.out(System.out, rs, query) ;	
         
        }
    }
	
}
